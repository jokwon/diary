<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" trimDirectiveWhitespaces="true"
	isErrorPage="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="authz"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ft" uri="http://www.anynori.com/taglibs/tags"%>
<%@ taglib prefix="ff" uri="http://www.anynori.com/taglibs/functions"%>
<%@ taglib prefix="compress"
	uri="http://htmlcompressor.googlecode.com/taglib/compressor"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<authz:authorize access="hasRole('ROLE_SUPER_ADMIN')">
	<authz:authentication property="principal.user" var="user"
		scope="session" />
</authz:authorize>

<compress:html enabled="true" compressCss="true">
<!DOCTYPE html>
<html lang="en-us">
<head>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>DDO DDI & GRRRRRRRR NI  & DIARY  </title>
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/smartadmin-production-plugins.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/smartadmin-production.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/smartadmin-skins.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/smartadmin-rtl.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/demo.min.css">
<link rel="shortcut icon" href="/smartAdmin/img/favicon/favicon.ico"type="image/x-icon">
<link rel="icon" href="/smartAdmin/img/favicon/favicon.ico"type="image/x-icon">
<link rel="stylesheet"href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
<link rel="apple-touch-icon"href="/smartAdmin/img/splash/sptouch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"href="/smartAdmin/img/splash/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"href="/smartAdmin/img/splash/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"href="/smartAdmin/img/splash/touch-icon-ipad-retina.png">
<link rel="apple-touch-startup-image"href="/smartAdmin/img/splash/ipad-landscape.png"media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image"href="/smartAdmin/img/splash/ipad-portrait.png"media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image"href="/smartAdmin/img/splash/iphone.png"media="screen and (max-device-width: 320px)">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="/smartAdmin/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="/smartAdmin/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="/smartAdmin/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="/smartAdmin/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="/smartAdmin/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<style type="text/css">
	.jqstooltip { 
				position: absolute;
				left: 0px;
				top: 0px;
				visibility: hidden;
				background: rgb(0, 0, 0) transparent;
				background-color: rgba(0,0,0,0.6);
				filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
				-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
				color: white;
				font: 10px arial, san serif;
				text-align: left;
				white-space: nowrap;
				padding: 5px;
				border: 1px solid white;
				z-index: 10000;}
	.jqsfield { color: white;
				font: 10px arial, san serif;
				text-align: left;}
</style>
<body class="mobile-detected pace-done">
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="menu" />
	<div id="main" role="main">
		<tiles:insertAttribute name="body" />
	</div>
	<tiles:insertAttribute name="footer" />
	<tiles:insertAttribute name="shortCut" />
	
<script type="text/javascript" asyncsrc="http://www.google-analytics.com/ga.js"></script>
<script data-pace-options="{ &quot;restartOnRequestAfter&quot;: true }"src="/smartAdmin/js/plugin/pace/pace.min.js"></script>
<script src="/smartAdmin/js/app.config.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="/smartAdmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<script src="/smartAdmin/js/bootstrap/bootstrap.min.js"></script>
<script src="/smartAdmin/js/notification/SmartNotification.min.js"></script>
<script src="/smartAdmin/js/smartwidgets/jarvis.widget.min.js"></script>
<script src="/smartAdmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="/smartAdmin/js/plugin/sparkline/jquery.sparkline.min.js"></script>
<script src="/smartAdmin/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<script src="/smartAdmin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<script src="/smartAdmin/js/plugin/select2/select2.min.js"></script>
<script src="/smartAdmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<script src="/smartAdmin/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<script src="/smartAdmin/js/plugin/fastclick/fastclick.min.js"></script>
<script src="/smartAdmin/js/demo.min.js"></script>
<script src="/smartAdmin/js/app.min.js"></script>
<script src="/smartAdmin/js/speech/voicecommand.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.manager.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.manager.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="/smartAdmin/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/smartAdmin/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="/smartAdmin/js/plugin/moment/moment.min.js"></script>
<script src="/smartAdmin/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="/smartAdmin/js/speech/voicecommand.min.js"></script>
<script>
			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();
			
				/*
				 * PAGE RELATED SCRIPTS
				 */

				
			});

		</script>	
	<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>
	
</body>
</html>
</compress:html>