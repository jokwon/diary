<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ft"  uri="http://www.anynori.com/taglibs/tags" %>
<%@ taglib prefix="ff"  uri="http://www.anynori.com/taglibs/functions" %>
<%@ taglib prefix="compress" uri="http://htmlcompressor.googlecode.com/taglib/compressor" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<compress:html enabled="true" compressCss="true">
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<!-- 		<meta http-equiv="Access-Control-Allow-Origin" content="*"/> -->
<!-- 		<meta http-equiv="Expires" content="-1"/> -->
<!-- 		<meta http-equiv="Pragma" content="no-cache"/> -->
<!-- 		<meta http-equiv="Cache-Control" content="no-cache"/> -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"> 
	<noscript>
		<meta http-equiv="refresh" content="0; url=http://www.enable-javascript.com/" />
	</noscript>
	<title>OrangeCast Admin</title>
	
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/panel.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/validatebox.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/combo.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/combobox.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/datagrid.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/propertygrid.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/pagination.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/linkbutton.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/tree.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.1.3.2/tabs.css" />
	
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
	<link type="text/css" rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/layout.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/skins.css" />
	

	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/layout-funzin.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/menu.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/funzin.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/jqtree.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/datepicker.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/spectrum.css" />
<%-- 		<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/easyui.css" /> --%>
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/select2.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/i-check.css" />
	<link type="text/css" rel="stylesheet" href="<ft:contextpath/>/css/video-js.css" />

	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/bootstrap.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/respond.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/app.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/funzin.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery-ui-1.10.4.custom.min.js"></script>
<%-- 		<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.easyui.min.js"></script> --%>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.easyui.1.3.5.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.draggable.1.3.5.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.droppable.1.3.5.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/isotope.pkgd.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.contextMenu.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.datepicker.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.fg.menu.js"></script>
<%-- 		<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.maskedinput.js"></script> --%>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.tree.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/widget.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/fastclick.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/select2.full.js"></script>
<%-- 		<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.slimscroll.js"></script> --%>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.scrollTo-1.4.3.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.layout-1.3.0.rc30.79.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/layout.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.inputmask.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.inputmask.extensions.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.inputmask.date.extensions.js"></script>
<%-- 		<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.inputmask.numeric.extensions.js"></script> --%>
<%-- 		<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.inputmask.phone.extensions.js"></script> --%>
<%-- 		<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.inputmask.regex.extensions.js"></script> --%>
<%-- 		<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/icheck.js"></script> --%>
	
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.easyui.datagrid.bufferview.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.easyui.datagrid.detailscrollview.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.easyui.datagrid.detailview.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.easyui.datagrid.groupview.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/jquery.easyui.datagrid.scrollview.js"></script>
	
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/js/video.js"></script>
	
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/ui/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/ui/ckeditor/adapters/jquery.js"></script>
	
	<script type="text/javascript">
	//<![CDATA[
		var contextPath = "<ft:contextpath/>";
	//]]>	
	</script>
	<script type="text/javascript" charset="utf-8" src="<ft:contextpath/>/ui/header.js"></script>
	<!--[if lt IE 9]>
		<style type="text/css">
			input[type=checkbox].skinned, input[type=radio].skinned {
				display:inline-block;
				position:static;
				height:auto !important;
				width:auto !important;
			}

			label.check {
				display:none !important;
			}
		</style>
	<![endif]-->
</head> 

<body class="hold-transition skin-blue layout-top-nav">

<div class="wrapper">
	<div class="content-wrapper">
		<tiles:insertAttribute name="body" />
	</div>
</div>
</body>
</html>
</compress:html>