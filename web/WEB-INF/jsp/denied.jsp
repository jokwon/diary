<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" buffer="48kb" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="ft" uri="http://www.anynori.com/taglibs/tags"%>
<%@ taglib prefix="ff" uri="http://www.anynori.com/taglibs/functions"%>
<%@ taglib prefix="compress" uri="http://htmlcompressor.googlecode.com/taglib/compressor" %>
<main class="main">
<sp:eval expression="@primeConfig['mode']" var="mode"/>
<div class="error-page">
	<h2 class="headline text-yellow"><c:out value="${mode == 'dev' ? '403' : ''}" /></h2>

	<div class="error-content">
		<h3 class="text-center">
		<h1>Forbidden</h1>
			<i class="fa fa-warning text-yellow"></i> 접근 권한이 없습니다.
		</h3>
		<h3 class="cbox" style="text-align: center;font-size:18px;">서비스 이용에 불편을 드려 죄송합니다.</h3>
		<p class="cbox" style="text-align: center;line-height:16px;font-size:16px;">
			방문하시려는 페이지에 대한 접근 권한이 없습니다.
		</p>
		<p class="cbox" style="text-align: center;line-height:14px;">
			입력하신 주소가 정확한지 다시 한번 확인해 주시기 바랍니다.
		</p>
	</div>
</div>
</main>