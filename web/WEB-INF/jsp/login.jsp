<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" buffer="48kb" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ft"  uri="http://www.anynori.com/taglibs/tags" %>
<%@ taglib prefix="ff"  uri="http://www.anynori.com/taglibs/functions" %>
<%@ taglib prefix="compress" uri="http://htmlcompressor.googlecode.com/taglib/compressor" %>
<compress:html enabled="true" compressCss="true">
<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="Access-Control-Allow-Origin" content="*"/>
		<meta http-equiv="Expires" content="-1"/>
		<meta http-equiv="Pragma" content="no-cache"/>
		<meta http-equiv="Cache-Control" content="no-cache"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/> 
		<noscript>
			<meta http-equiv="refresh" content="0; url=http://www.enable-javascript.com/" />
		</noscript>
		<title>Prime</title>
		<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/smartadmin-production-plugins.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/smartadmin-production.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/smartadmin-skins.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/smartadmin-rtl.min.css">
<link rel="stylesheet" type="text/css" media="screen"href="/smartAdmin/css/demo.min.css">
<link rel="shortcut icon" href="/smartAdmin/img/favicon/favicon.ico"type="image/x-icon">
<link rel="icon" href="/smartAdmin/img/favicon/favicon.ico"type="image/x-icon">
<link rel="stylesheet"href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">
<link rel="apple-touch-icon"href="/smartAdmin/img/splash/sptouch-icon-iphone.png">
<link rel="apple-touch-icon" sizes="76x76"href="/smartAdmin/img/splash/touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="120x120"href="/smartAdmin/img/splash/touch-icon-iphone-retina.png">
<link rel="apple-touch-icon" sizes="152x152"href="/smartAdmin/img/splash/touch-icon-ipad-retina.png">
<link rel="apple-touch-startup-image"href="/smartAdmin/img/splash/ipad-landscape.png"media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image"href="/smartAdmin/img/splash/ipad-portrait.png"media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image"href="/smartAdmin/img/splash/iphone.png"media="screen and (max-device-width: 320px)">



<script type="text/javascript" asyncsrc="http://www.google-analytics.com/ga.js"></script>
<script data-pace-options="{ &quot;restartOnRequestAfter&quot;: true }"src="/smartAdmin/js/plugin/pace/pace.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="/smartAdmin/js/app.config.js"></script>
<script src="/smartAdmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>
<script src="/smartAdmin/js/bootstrap/bootstrap.min.js"></script>
<script src="/smartAdmin/js/notification/SmartNotification.min.js"></script>
<script src="/smartAdmin/js/smartwidgets/jarvis.widget.min.js"></script>
<script src="/smartAdmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="/smartAdmin/js/plugin/sparkline/jquery.sparkline.min.js"></script>
<script src="/smartAdmin/js/plugin/jquery-validate/jquery.validate.min.js"></script>
<script src="/smartAdmin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>
<script src="/smartAdmin/js/plugin/select2/select2.min.js"></script>
<script src="/smartAdmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>
<script src="/smartAdmin/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>
<script src="/smartAdmin/js/plugin/fastclick/fastclick.min.js"></script>
<script src="/smartAdmin/js/demo.min.js"></script>
<script src="/smartAdmin/js/app.min.js"></script>
<script src="/smartAdmin/js/speech/voicecommand.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.manager.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.manager.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.tooltip.min.js"></script>
<script src="/smartAdmin/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/smartAdmin/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="/smartAdmin/js/plugin/moment/moment.min.js"></script>
<script src="/smartAdmin/js/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>
<script src="/smartAdmin/js/speech/voicecommand.min.js"></script>
	</head> 
	<%-- <body class="app flex-row align-items-center  pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="transform: translate3d(100%, 0px, 0px);">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                  <form name="loginForm" action="/login" method="post">
                <h1>Login</h1>
                <p class="text-muted">Sign In to your account</p>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div>
                  <input class="form-control" type="text" placeholder="id" name="userId">
                  <form:errors path="userId" element="blockquote" class="error warnMessage" /> 
                </div>
                <div class="input-group mb-4">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-lock"></i>
                    </span>
                  </div>
                  <input class="form-control" type="password" placeholder="Password" name="userPassword" >
                   <form:errors path="password" element="blockquote" class="error warnMessage" />
                </div>
                <div class="row">
                  <div class="col-6">
                    <input type="submit" class="btn btn-primary px-4" value="Login" data-fz-click="Login" data-fz-click-propagation="true">
                  </div>
                  <div class="col-6 text-right">
                    <button class="btn btn-link px-0" type="button">Forgot password?</button>
                  </div>
                </div>
                  </form>
              </div>
            </div>
            <div class="card text-white bg-primary py-5 d-md-down-none" style="width:44%">
              <div class="card-body text-center">
                <div>
                  <h2>Sign up</h2>
                  <p>안녕하십니까 프라임 방재 입니다.</p><p>회원가입 후 사용해 주시기 바랍니다.</p>
                  <button class="btn btn-primary active mt-3" type="button">Register Now!</button>
                </div>
              </div>
            </div>
          </div>
    <!-- Bootstrap and necessary plugins-->
        </div>
      </div>
    </div>
</body>
  	</body> --%>
  	
  	
  	
  	
  	
  	
  	<body class="animated fadeInDown  mobile-detected pace-done">
		<header id="header">

			<div id="logo-group">
				<span id="logo"> <img src="/smartAdmin/img/logo.png" alt="SmartAdmin"> </span>
			</div>

			<span id="extr-page-header-space"> <span class="hidden-mobile hiddex-xs">Need an account?</span> <a href="register.html" class="btn btn-danger">Create account</a> </span>

		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
						<h1 class="txt-color-red login-header-big">SmartAdmin</h1>
						<div class="hero">

							<div class="pull-left login-desc-box-l">
								<h4 class="paragraph-header">It's Okay to be Smart. Experience the simplicity of SmartAdmin, everywhere you go!</h4>
								<div class="login-app-icons">
									<a href="javascript:void(0);" class="btn btn-danger btn-sm">Frontend Template</a>
									<a href="javascript:void(0);" class="btn btn-danger btn-sm">Find out more</a>
								</div>
							</div>
							
							<img src="/smartAdmin/img/demo/iphoneview.png" class="pull-right display-image" alt="" style="width:210px">

						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h5 class="about-heading">About SmartAdmin - Are you up to date?</h5>
								<p>
									Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.
								</p>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<h5 class="about-heading">Not just your average template!</h5>
								<p>
									Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi voluptatem accusantium!
								</p>
							</div>
						</div>

					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
						<div class="well no-padding">
							<form name="loginForm" action="/login" method="post" id="login-form" class="smart-form client-form" novalidate="novalidate">
								<header>
									Sign In
								</header>

								<fieldset>
									
									<section>
										<label class="label">E-mail</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" placeholder="id" name="userId" autocomplete="off">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
									</section>

									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" placeholder="Password" name="userPassword" autocomplete="off">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<div class="note">
											<a href="forgotpassword.html">Forgot password?</a>
										</div>
									</section>

									<section>
										<label class="checkbox">
											<input type="checkbox" name="remember" checked="">
											<i></i>Stay signed in</label>
									</section>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Sign in
									</button>
								</footer>
							</form>

						</div>
						
						<h5 class="text-center"> - Or sign in using -</h5>
															
							<ul class="list-inline text-center">
								<li>
									<a href="javascript:void(0);" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
								</li>
								<li>
									<a href="javascript:void(0);" class="btn btn-warning btn-circle"><i class="fa fa-linkedin"></i></a>
								</li>
							</ul>
						
					</div>
				</div>
			</div>

		</div>
</body>
  	<script type="text/javascript">
			$(document).ready(function(){
				$("#login-form").validate({
					// Rules for form validation
					rules : {
						email : {
							required : true,
							email : true
						},
						password : {
							required : true,
							minlength : 3,
							maxlength : 20
						}
					},

					// Messages for form validation
					messages : {
						email : {
							required : 'Please enter your email address',
							email : 'Please enter a VALID email address'
						},
						password : {
							required : 'Please enter your password'
						}
					},

					// Do not change code below
					errorPlacement : function(error, element) {
						error.insertAfter(element.parent());
					}
				});
				var msg = "";
				if( -1 < (document.location+"").indexOf("login_error=failure") ){
					//$('#message').html('<img src="<ft:contextpath/>/images/icons/error.gif" class="middle"/>&nbsp;&nbsp;ID/PW를 다시 확인하여 주십시오.');\
					//msg = "비밀번호가 일치하지 않습니다.";
					msg = "ID/PW를 다시 확인하여 주십시오.";
				} else if( -1 < (document.location+"").indexOf("login_error=denied") ){
					//$('#message').html('<img src="<ft:contextpath/>/images/icons/error.gif" class="middle"/>&nbsp;&nbsp;접근 권한이 없습니다. 관리자에게 문의해주세요.');
					msg = "접근권한이 없습니다.";
				} /* else if( -1 < (document.location+"").indexOf("login_error=nonexistent") ) {
					msg = "등록되지 않은 ID입니다.";
				} */
				if (-1 < (document.location+"").indexOf("login_error=failure") || -1 < (document.location+"").indexOf("login_error=denied") /*|| -1 < (document.location+"").indexOf("login_error=nonexistent") */) {
					alert(msg);
				}
			});
			function Login() {
				var id = $(document).find('input[name=userId]');
				var pswd = $(document).find('input[name=userPassword]');
				if (''.equals(id.val().trim()) || ''.equals(pswd.val().trim())) {
					$('#message').html('<img src="<ft:contextpath/>/images/icons/error.gif" class="middle"/>&nbsp;&nbsp;아이디와 비밀번호를 입력해 주십시오.');
					if (''.equals(id.val().trim())) {
						id.focus();
						return false;
					};
					if (''.equals(pswd.val().trim())) {
						pswd.focus();
						return false;
					};
				};
				document.loginForm.target = '_self';
				console.log("id:" + id + ", pswd:" + pswd );
				document.loginForm.action = '<ft:contextpath/>/login';
				document.loginForm.submit();
			}
			function sendEmail() {
				// TO DO
			}
		</script>
  	
</html>
</compress:html>