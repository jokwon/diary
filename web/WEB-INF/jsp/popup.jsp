<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ft"  uri="http://www.anynori.com/taglibs/tags" %>
<%@ taglib prefix="ff"  uri="http://www.anynori.com/taglibs/functions" %>
<%@ taglib prefix="compress" uri="http://htmlcompressor.googlecode.com/taglib/compressor" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<compress:html enabled="true" compressCss="true">

<!-- S:: setting popup style1 -->			
		<div id="user_setting" class="overlay off" >
			<div class="pop_cap">
				<div class="popup_st1" style="height:auto;">
					<h2 class="title">설정</h2>
					<a class="close" >&times;</a>
					<div class="scrollcap" style="height:auto;">
						<div class="content">
							<h3>개인정보 설정</h3>
							<div class="user_photo">
								<img src="images/user_df.svg"/>
								<div >
									<input class="upload_file" type="file" value=""/>
								</div>
							</div>
							<ul>
								<li><h3>닉네임</h3><input type="text"  class="input_w10p" /></li>
								<li><h3>E-mail</h3><input type="text"  class="input_w10p" /></li>
								<li><h3>비밀번호</h3><input type="password" class="input_w10p" /></li>
								<li><h3>비밀번호 확인</h3><input type="password" class="input_w10p" /></li>
							</ul>
							<a class="bt_01 color_b" onclick="pop_close()">취소</a>
							<a class="bt_01 color_a" href="#">완료</a>
							</div>
					</div>
				</div>
			</div>
		</div>
		<!-- E:: setting popup style 1//-->
		<!-- S:: write popup style1 -->			
		<div id="write_play" class="overlay off" >
			<div class="pop_cap">
				<div class="popup_st3"  >
					<h2 class="title">글쓰기</h2>
					<a class="close" >&times;</a>
					<div class="scrollcap">
						<div class="content">
							<div class="user_images">
								<img src="/images/images_01.jpg"/>
							</div>
							<ul>
								<li>
									<button class="upload_bt" >사진첨부</button>
									<input class="upload_file" type="file" value=""/>
								</li>
								<li>
									<input type="text" placeholder="제목을 입력해 주세요."  class="input_w10p" />
								</li>
								<li>
									<textarea placeholder="내용을 입력해 주세요." class="tarea_w100p" ></textarea>
								</li>
							</ul>
							<a class="bt_01 color_b" onclick="write_play_close()">취소</a>
							<a class="bt_01 color_a" href="#">완료</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- E:: write popup style 1//-->
		
		<!-- S::어항 추가 -->			
			<div id="tanks"  class="overlay off" >
			<div class="pop_cap">
				<div class="popup_st1"  >
					<h2 class="title txtc_blue">TANK INFO</h2>
					<a class="close" >&times;</a>
					<div class="scrollcap">
						<div class="content">
							<div class="user_images">
								<img src="/imgs/image_df.svg" id="tankPreviewImg"/>
							</div>
							
			<form action="/tank/insert" method="post" enctype="multipart/form-data" name="tankForm">
							<ul>
								<li>
									<button class="upload_bt" >사진첨부</button>
									<input type="file" name = "file" id="tankPhoto"style="opacity:1;box-sizing:border-box;-webkit-appearance:none !important;cursor:pointer;width:100%;font-size: 12px; color:#666;   border: 1px solid #ddd;    background: #fff;line-height:30px;height:30px;padding:3px 2px;margin-top:-50px;margin-left:8px;" />
								</li>
								<li><h3>TITLE :</h3><input type="text" name="title" placeholder="Title"  class="input_w10p" style="width:69.5%;"/></li>
								<li>
									<h3>SIZE(cm) :</h3>
									<input type="text" name="width" placeholder="Width" class="input_w10p"  style="width:20%;"/>
									<span>X</span>
									<input type="text" name="height" placeholder="Height" class="input_w10p" style="width:20%;"/>
									<span>X</span>
									<input type="text" name="depth" placeholder="Depth"  class="input_w10p" style="width:20%;"/>
									
								</li>
								<!-- 대표어항 설정시 Y, 그렇지 않으면 N  -->
								<li><input type="hidden" name = "checkRepYn" placeholder="탱크아이디"  class="input_w10p"/></li>
								
								<li>
									<h3>종류:</h3>
									<select name="capacity" class="select_w100p" style="width:69.5%;">
										<option value="1">해수어</option>
										<option value="2">산호</option>
										<option value="3">종합</option>
								</select>
								</li>
								<li>
									<h3>설명:</h3><textarea placeholder="설명을 입력해 주세요." class="tarea_w100p" name="contents"></textarea>
								</li>
								<li><h3>대표어항 설정</h3><input name="repYn" id="repYn"class="chk" value="Y" type="checkbox" style="float:left; margin-left:26px; margin-top:3px;" /></li>
							</ul>
							<h3>내 어항 설정</h3>
						 	 <input type="hidden" name ="reefTankElementLists[0].elementsId" id="elementsId0" />
							<input type="hidden" name= "reefTankElementLists[1].elementsId" id="elementsId1" />
							<input type="hidden"  name = "reefTankElementLists[2].elementsId" id="elementsId2" />
							<table class="table_list">
							
								<tbody>
									<tr class="title">
									<!-- 	<td><h3>사용<br>여부</h3></td> -->
										<td><h3>순번</h3></td>
										<td><h3>카테고리</h3></td>
										<td><h3>공개여부</h3></td>
									</tr>
									<tr>
										<!-- <td><input class="chk" type="checkbox" /></td> -->
										
										<td>
											<select class="select_w10p" name = "reefTankElementLists[0].rank" id = "rank0">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
											</select>
										</td>
										<td>
											<select class="select_w100p" name ="reefTankElementLists[0].type" id = "type0">
												<option value="Fish">fishes</option>
												<option value="Coral">corals</option>
												<option value="Equip">equipments</option>
											</select>
										</td>
										<td>
											<select class="select_w30p" name="reefTankElementLists[0].openYn" id = "openYn0">
												<option value="Y">공개</option>
												<option value="N">비공개</option>
											</select>
										</td>
									</tr>
									<tr>
										<!-- <td><input class="chk" type="checkbox" /></td> -->
										
										<td>
											<select class="select_w10p" name = "reefTankElementLists[1].rank" id = "rank1">
												<option value="2">2</option>
												<option value="1">1</option>
												<option value="3">3</option>
											</select>
										</td>
										<td>
											<select class="select_w100p" name = "reefTankElementLists[1].type" id = "type1">
												<option value="Coral">corals</option>
												<option value="Fish">fishes	</option>
												<option value="Equip">equipments</option>
											</select>
										</td>
										<td>
											<select class="select_w30p" name ="reefTankElementLists[1].openYn" id = "openYn1">
												<option value="Y">공개</option>
												<option value="N">비공개</option>
											</select>
										</td>
									</tr>
									<tr>
										<!-- <td><input class="chk" type="checkbox" /></td> -->
										
										<td>
											<select class="select_w10p" name = "reefTankElementLists[2].rank" id = "rank2">
												<option value="3">3</option>
												<option value="1">1</option>
												<option value="2">2</option>	
											</select>
										</td>
										<td>
											<select class="select_w100p" name="reefTankElementLists[2].type" id = "type2">
												<option value="Equip">equipments</option>
												<option value="Fish">fishes</option>
												<option value="Coral">corals</option>
											</select>
										</td>
										<td>
											<select class="select_w30p" name ="reefTankElementLists[2].openYn" id="openYn2">
												<option value="Y">공개</option>
												<option value="N">비공개</option>
											</select>
										</td>
									</tr>
								</tbody>
							</table>
							
							
							<input type="hidden" name="tankId" />
							<input class="bt_01a color_b" value="취소" onclick="add_item_close('tanks')"/>
							<input class="bt_submit_01 color_a" type="submit" value="완료" id="writeBt"/>
							<a class="bt_01 color_b" id="tankDelete" onclick="tankDelete()">삭제</a>
				</form>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- E::어항 추가 -->
		<!-- S::fishes 추가 -->
		<div id="fishes"  class="overlay off" >
			<div class="pop_cap">
				<div class="popup_st3"  >
					<h2 class="title txtc_blue">Add fishes</h2>
					<a class="close" >&times;</a>
					<div class="scrollcap">
						<div class="content">
							<div class="user_images">
								<img src="/images/images_01.jpg" id = "fishPreviewImg" style="width:100%; height:100%;" class = "previewElementsInfoImg"/>
							</div>
					<form action="/tankElementsInfo/insert" method="post" enctype="multipart/form-data" name="tankInfoForm">
							<ul>
								<li>
									<button class="upload_bt" >사진첨부</button>
									<input class="upload_file" type="file" title = "대표이미지를 등록해주세요." name = "file" id = "fishCustomPhoto">
								</li>
								<li>
									<select class="select_w100p" name="lCode" id = "fishCode">
										<option value="">종류</option>
									</select>
								</li>
								<li>
									<select class="select_w100p" name="mCode" id = "fishMCode">
										<option value="">종류</option>
									</select>
								</li>
								<li><input type="text" name = "aliasName" placeholder="특징 및 별칭"  class="input_w10p" /></li>
								<li><input type="text" name = "buyDates" placeholder="구입일자(yyyy-mm-dd)"  class="input_w10p" alt="date"/></li>
								<li><input type="text" name = "buyCost" placeholder="구입가격"  class="input_w10p" alt="decimal-us"/></li>
								<li><input type="text" name = "shopName" placeholder="구입처"  class="input_w10p" /></li>
								<!--<li><input type="text" name = "linkUrl" placeholder="관련링크"  class="input_w10p" /></li> -->
								<!--S::해당 엘리먼트 정보를 가져오기 위한 Param  -->
								<li><input type="hidden" name = "elementsInfoIdFromPopup"  class="input_w10p" /></li>
								<li><input type="hidden" name = "fromTankId" placeholder="탱크아이디"  class="input_w10p" value="${repTankId}"/></li>
								<li><input type="hidden" name = "elementsId" placeholder="탱크아이디"  class="input_w10p" value="${reefTankElements.elementsId}"/></li>
								<li><input type="hidden" name = "elementsType" placeholder="탱크아이디"  class="input_w10p" value="Fish"/></li>
								
								
								<!-- <li><input type="hidden" name = "types" value="1" placeholder="엘리먼트 타입"  class="input_w10p" /></li> -->
								<!--E::해당 엘리먼트 정보를 가져오기 위한 Param  -->
							</ul>
							<a class="bt_01 color_b" onclick="add_item_close('fishes')">취소</a>
							<input class="bt_submit_01 color_a" type="submit" value="완료"/>
							<a class="bt_01 color_b" id = "fishDelete" onclick="infoDelete()">삭제</a>
					</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- E::fishes 추가 -->
		<div id="corals"  class="overlay off" >
			<div class="pop_cap">
				<div class="popup_st3 "  >
					<h2 class="title txtc_orange">Add corals</h2>
					<a class="close" >&times;</a>
					<div class="scrollcap">
						<div class="content">
							<div class="user_images">
								<img src="/images/images_01.jpg" id = "coralPreviewImg" style="width:100%; height:100%;" class = "previewElementsInfoImg"/>
							</div>
				<form action="/tankElementsInfo/insert" method="post" enctype="multipart/form-data" name="tankInfoForm">
							<ul>
								<li>
									<button class="upload_bt" >사진첨부</button>
									<input class="upload_file" type="file" title = "대표이미지를 등록해주세요." name = "file" id = "coralCustomPhoto">
								</li>
								<li>
									<select class="select_w100p" name="lCode" id = "coralCode">
										<option value="">종류</option>
									</select>
								</li>
								<li>
									<select class="select_w100p" name="mCode" id = "coralMCode">
										<option value="">종류</option>
									</select>
								</li>
								<li><input type="text" name = "aliasName" placeholder="특징 및 별칭"  class="input_w10p" /></li>
								<li><input type="text" name = "buyDates" placeholder="구입일자(yyyy-mm-dd)"  class="input_w10p" alt="date"/></li>
								<li><input type="text" name = "buyCost" placeholder="구입가격"  class="input_w10p" alt="decimal-us"/></li>
								<li><input type="text" name = "shopName" placeholder="구입처"  class="input_w10p" /></li>
							<!-- 	<li><input type="text" name = "productExplain" placeholder="설명"  class="input_w10p" /></li>
								<li><input type="text" name = "linkUrl" placeholder="관련링크"  class="input_w10p" /></li> -->
								
								<!--S::해당 엘리먼트 정보를 가져오기 위한 Param  -->
								<li><input type="hidden" name = "elementsInfoIdFromPopup"  class="input_w10p" /></li>
								<li><input type="hidden" name = "fromTankId" placeholder="탱크아이디"  class="input_w10p" value="${repTankId}"/></li>
								<li><input type="hidden" name = "elementsId" placeholder="탱크아이디"  class="input_w10p" value="${reefTankElements.elementsId}"/></li>
								<li><input type="hidden" name = "elementsType" placeholder="탱크아이디"  class="input_w10p" value="Coral"/></li>
								<!--E::해당 엘리먼트 정보를 가져오기 위한 Param  -->
							</ul>
							<a class="bt_01 color_b" onclick="add_item_close('corals')">취소</a>
							<input class="bt_submit_01 color_a" type="submit" value="완료" />
							<a class="bt_01 color_b" id = "coralDelete" onclick="infoDelete()">삭제</a>
					</form>
					
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="equipment"  class="overlay off" >
			<div class="pop_cap">
				<div class="popup_st3 "  >
					<h2 class="title txtc_green">Add equipment</h2>
					<a class="close" >&times;</a>
					<div class="scrollcap">
						<div class="content">
							<div class="user_images">
								<img src="/images/images_01.jpg" id = "equipPreviewImg" style="width:100%; height:100%;" class = "previewElementsInfoImg"/>
							</div>
				<form action="/tankElementsInfo/insert" method="post" enctype="multipart/form-data" name="tankInfoForm">
							<ul>
								<li>
									<button class="upload_bt" >사진첨부</button>
									<input class="upload_file" type="file" title = "대표이미지를 등록해주세요." name = "file" id = "equipCustomPhoto">
								</li>
								<li>
									<select class="select_w100p" name="lCode" id = "equipCode">
										<option value="">종류</option>
									</select>
								</li>
								<li>
									<select class="select_w100p" name="mCode" id = "equipMCode">
										<option value="">종류</option>
									</select>
								</li>
								<li><input type="text" name = "aliasName" placeholder="특징 및 별칭"  class="input_w10p" /></li>
								<li><input type="text" name = "buyDates" placeholder="구입일자(yyyy-mm-dd)"  class="input_w10p" alt="date"/></li>
								<li><input type="text" name = "buyCost" placeholder="구입가격"  class="input_w10p" alt="decimal-us"/></li>
								<li><input type="text" name = "shopName" placeholder="구입처"  class="input_w10p" /></li>
								<!-- <li><input type="text" name = "productExplain" placeholder="설명"  class="input_w10p" /></li>
								<li><input type="text" name = "linkUrl" placeholder="관련링크"  class="input_w10p" /></li> -->
								
								<!--S::해당 엘리먼트 정보를 가져오기 위한 Param  -->
								<li><input type="hidden" name = "elementsInfoIdFromPopup"  class="input_w10p" /></li>
								<li><input type="hidden" name = "fromTankId" placeholder="탱크아이디"  class="input_w10p" value="${repTankId}"/></li>
								<li><input type="hidden" name = "elementsId" placeholder="탱크아이디"  class="input_w10p" value="${reefTankElements.elementsId}"/></li>
								<li><input type="hidden" name = "elementsType" placeholder="탱크아이디"  class="input_w10p" value="Equip"/></li>
								<!--E::해당 엘리먼트 정보를 가져오기 위한 Param  -->
							</ul>
							<a class="bt_01 color_b" onclick="add_item_close('equipment')">취소</a>
							<input class="bt_submit_01 color_a" type="submit" value="완료"/>
							<a class="bt_01 color_b" id="equipDelete" onclick="infoDelete()">삭제</a>
				</form>			
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="tank_status"  class="overlay off" >
			<div class="pop_cap">
				<div class="popup_st3 " style="max-height: 400px!important;max-width:340px!important" >
					<h2 class="title txtc_status">Tank status</h2>
					<a class="close" >&times;</a>
					<div class="scrollcap">
						<div class="content">
						<form action="/tankParam/insert" method="post" enctype="multipart/form-data" name="tankParamForm">
							<ul>
								<!--<li>
									<select class="select_w100p">
										<option value="">측정일자</option>
										<option value="">yyyy-mm-dd</option>
										<option value="">yyyy-mm-dd</option>
										<option value="">yyyy-mm-dd</option>
										<option value="">...</option>
									</select>
								</li-->
								<!--  <li>
									<select class="select_w100p">
										<option value="">측정종류</option>
										<option value="">option1</option>
										<option value="">option2</option>
										<option value="">option3</option>
										<option value="">...</option>
									</select>
								</li>-->
									 <li><input type="text" name ="measureDates" placeholder="측정일자 (yyyy-MM-dd)" class="input_w10p"/></li>  
								<li>
									<select class="select_w100p" name="codeId" id = "codeId">
										<option value="">파라미터를 선택하세요.</option>
										<c:forEach items="${reefTankParamCodeList}" var= "reefTankParamCodeList">
										<option value="${reefTankParamCodeList.paramCodeId}">${reefTankParamCodeList.paramName}</option>
										</c:forEach>
									</select>
								</li>
								<li><input type="text" placeholder="측정값" name = "value" class="input_w10p" value="측정값 : 999,999" /></li>
								<li><input type="text" placeholder="이전 측정값" name="beforeValue" readonly class="input_w10p color_b" value="이전 측정값 : 999,999" /></li> 
								<li><input type="text" placeholder="탱크아이디" name = "fromTankId" class="input_w10p" value="${repTankId}"/></li>
								<li><a class="update_bt" >컨트롤러 데이타 가져오기</a></li>
							</ul>
							<a class="bt_01 color_b" onclick="add_item_close('tank_status')">취소</a>
							<input class="bt_submit_01 color_a" type="submit" value="완료" />
							<!-- <a class="bt_01 color_a " >초기화</a> -->
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- E:: item popup style 1//-->

		<!-- S::popup alert -->			
		<div id="popup_logout" class="overlay2" >
			<div class="pop_cap2">
				<div class="popup_alert" >
					<h2>로그아웃 하시겠습니까?</h2>
					<div class="content">
						<a href="#" class="txt_blue">취소</a>
						<a href="#" onclick="logout()" class="txt_red">로그아웃</a>
					</div>
				</div>
			</div>
		</div>
		<!-- E::popup alert -->	
		<div id="view_detail" class="overlay3 off" >
			<div class="pop_cap2">
				<div class="popup_alert" >
					<div class="content">
						<ul>
							<li><h4>2107-01-01 <span> Nickname1 </span> 99,999원</h4>	<a class="del_item" >&times;</a></li>
							<li><h4>2107-12-31 <span> Nickname2 </span> 999,999원</h4><a class="del_item" >&times;</a></li>
							<li><h4>2107-12-31 <span> Nickname3 </span> 999,999원</h4><a class="del_item" >&times;</a></li>
							<li><h4>2107-12-31 <span> Nickname4 </span> 999,999원</h4><a class="del_item" >&times;</a></li>
						</ul>
						<a  onclick="view_detail_off()" class="txt_blue">Cancel</a>
						<a  onclick="" class="txt_red">SAVE</a>
					</div>
				</div>
			</div>
			</div>
			</compress:html>
			
			
			
			
			
			
			
