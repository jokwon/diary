<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ft"  uri="http://www.anynori.com/taglibs/tags" %>
<%@ taglib prefix="ff"  uri="http://www.anynori.com/taglibs/functions" %>
<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<ol class="breadcrumb">
					<li>Home</li><li>Tables</li><li>Data Tables</li>
				</ol>

			</div>
<div id="content">

				<!-- Bread crumb is created dynamically -->
				<!-- row -->
				<div class="row">
				
					<!-- col -->
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h1 class="page-title txt-color-blueDark"><!-- PAGE HEADER --><i class="fa-fw fa fa-puzzle-piece"></i> App Views <span>&gt;
							Profile </span></h1>
					</div>
					<!-- end col -->
				
					<!-- right side of the page with the sparkline graphs -->
					<!-- col -->
					<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
						<!-- sparks -->
						<ul id="sparks">
							<li class="sparks-info">
								<h5> My Income <span class="txt-color-blue">$47,171</span></h5>
								<div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm"><canvas width="89" height="26" style="display: inline-block; width: 89px; height: 26px; vertical-align: top;"></canvas></div>
							</li>
							<li class="sparks-info">
								<h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up" data-rel="bootstrap-tooltip" title="Increased"></i>&nbsp;45%</span></h5>
								<div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm"><canvas width="82" height="26" style="display: inline-block; width: 82px; height: 26px; vertical-align: top;"></canvas></div>
							</li>
							<li class="sparks-info">
								<h5> Site Orders <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;2447</span></h5>
								<div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm"><canvas width="82" height="26" style="display: inline-block; width: 82px; height: 26px; vertical-align: top;"></canvas></div>
							</li>
						</ul>
						<!-- end sparks -->
					</div>
					<!-- end col -->
				
				</div>
				<!-- end row -->
				
				<!-- row -->
				
				<div class="row">
				
					<div class="col-sm-12">
				
				
							<div class="well well-sm">
				
								<div class="row">
				
									<div class="col-sm-12 col-md-12 col-lg-12">
										<div class="well well-light well-sm no-margin no-padding">
				
											<div class="row">
				
												<div class="col-sm-12">
													<div id="myCarousel" class="carousel fade profile-carousel">
														<div class="carousel-inner">
															<div class="item active">
														
																<img src="/smartAdmin/img/demo/s1.jpg" alt="demo user" style="width:100%;">
																
															</div>   
														</div>
													</div>
												</div>
				
												<div class="col-sm-12">
				
													<div class="row">
				
														<div class="col-sm-2 profile-pic">
															<center>
															<img src="/smartAdmin/img/superbox/superbox-full-1.jpg" alt="demo user" style="top:0px; max-width:140px; height:200px; border: 0px solid #fff; margin-top:10px;">
															</center>
														</div>
														<div class="col-sm-3">
															<h1>John <span class="semi-bold">Doe</span> (91842521)
															<br>
															<small> CEO, SmartAdmin</small></h1>
				
															<ul class="list-unstyled">
																<li>
																	<p class="text-muted">
																		<i class="fa fa-phone"></i>&nbsp;&nbsp;(<span class="txt-color-darken">313</span>) <span class="txt-color-darken">464</span> - <span class="txt-color-darken">6473</span>
																	</p>
																</li>
																<li>
																	<p class="text-muted">
																		<i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:simmons@smartadmin">ceo@smartadmin.com</a>
																	</p>
																</li>
																<li>
																	<p class="text-muted">
																		<i class="fa fa-skype"></i>&nbsp;&nbsp;<span class="txt-color-darken">john12</span>
																	</p>
																</li>
																<li>
																	<p class="text-muted">
																		<i class="fa fa-calendar"></i>&nbsp;&nbsp;<span class="txt-color-darken">Free after <a href="javascript:void(0);" rel="tooltip" title="" data-placement="top" data-original-title="Create an Appointment">4:30 PM</a></span>
																	</p>
																</li>
															</ul>
															<br>
															<a href="javascript:void(0);" class="btn btn-default btn-xs"><i class="fa fa-envelope-o"></i> Send Message</a>
															<br>
															<br>
				
														</div>
				
													</div>
				
												</div>
				
											</div>
				
											<div class="row">
				
												<div class="col-sm-12">
				
													<hr>
				
													<div class="padding-10">
				
													<table class="table table-hover">
												<tbody>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td style="width:10%;"><a href="javascript:void(0);">부서명</a></td>
														<td>가공팀</td>
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">종전부서</a></td>
														<td>후가공팀</td>
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">사용권한</a></td>
														<td>작업자</td>
														
															
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">최종학력</a></td>
														<td>학사</td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">최종졸업학교</a></td>
														<td>인천대학교</td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">자격증</a></td>
														<td>컴퓨터활용능력 1급, 정보처리기사, 컴퓨터활용능력 2급</td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">입사일</a></td>
														<td>2017.08.31</td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">퇴사일</a></td>
														<td></td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">보직변경 발령일</a></td>
														<td>2018.10.11</td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">최종 휴직일</a></td>
														<td></td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">최종 복직일</a></td>
														<td></td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">특이사항</a></td>
														<td>성격이 모났음</td>
														
														
													</tr>
													<tr>
														<td class="text-center"><strong>*</strong></td>
														<td><a href="javascript:void(0);">퇴사일</a></td>
														<td></td>
														
														
														
													</tr>
													<tr>
														<td colspan="3"></td>
													</tr>
													<tr>
														<td></td>
														<td></td>
														<td></td>
													</tr>
												</tbody>
											</table>
				
													</div>
				
												</div>
				
											</div>
											<!-- end row -->
				
										</div>
				
									</div>
								</div>
				
							</div>
				
				
					</div>
				
				</div>
				
				<!-- end row -->

			</div>
<script type="text/javascript">
$(document).ready(function() {
	$('a[href="/user/userList"]').parent().addClass("active");
	$('a[href="/user/userList"]').parent().parent().parent().addClass("open");
	$('#birth_date').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		
	});
	$('#join_date').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		
	});
	$('#leave_date').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		
	});
	$('#rest_startdate').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		onSelect: function (selectedDate) {
			$('#finishdate').datepicker('option', 'minDate', selectedDate);
		}
	});
	$('#rest_finishdate').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		onSelect: function (selectedDate) {
			$('#startdate').datepicker('option', 'maxDate', selectedDate);
		}
	});
	$('#rejoin_date').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		
	});
			
		});
			
</script>
