<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ft"  uri="http://www.anynori.com/taglibs/tags" %>
<%@ taglib prefix="ff"  uri="http://www.anynori.com/taglibs/functions" %>
<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Home</li><li>Tables</li><li>Data Tables</li>
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			
<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h1 class="page-title txt-color-blueDark">
							<i class="fa fa-table fa-fw "></i> 
								Table 
							<span>&gt; 
								Data Tables
							</span>
						</h1>
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
						<ul id="sparks" class="">
							<li class="sparks-info">
								<a href="javascript:void(0);" class="btn bg-color-blueDark txt-color-white" id="userWriteBtn">직원등록</a>
							</li>
						</ul>
					</div>
				</div>
				
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
							<div class="jarviswidget jarviswidget-color-blueDark jarviswidget-sortable" id="wid-id-1" data-widget-editbutton="false" role="widget">
								<header role="heading"><div class="jarviswidget-ctrls" role="menu"> 
									  <a href="javascript:void(0);" class="button-icon jarviswidget-toggle-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Collapse"><i class="fa fa-minus "></i></a> 
									  <a href="javascript:void(0);" class="button-icon jarviswidget-fullscreen-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Fullscreen"><i class="fa fa-expand "></i></a> 
									  <a href="javascript:void(0);" class="button-icon jarviswidget-delete-btn" rel="tooltip" title="" data-placement="bottom" data-original-title="Delete"><i class="fa fa-times"></i></a></div>
									  <div class="widget-toolbar" role="menu">
									  <a data-toggle="dropdown" class="dropdown-toggle color-box selector" href="javascript:void(0);"></a>
									  	<ul class="dropdown-menu arrow-box-up-right color-select pull-right">
									  		<li><span class="bg-color-green" data-widget-setstyle="jarviswidget-color-green" rel="tooltip" data-placement="left" data-original-title="Green Grass"></span></li>
									  		<li><span class="bg-color-greenDark" data-widget-setstyle="jarviswidget-color-greenDark" rel="tooltip" data-placement="top" data-original-title="Dark Green"></span></li>
									  		<li><span class="bg-color-greenLight" data-widget-setstyle="jarviswidget-color-greenLight" rel="tooltip" data-placement="top" data-original-title="Light Green"></span></li>
									  		<li><span class="bg-color-purple" data-widget-setstyle="jarviswidget-color-purple" rel="tooltip" data-placement="top" data-original-title="Purple"></span></li>
									  		<li><span class="bg-color-magenta" data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip" data-placement="top" data-original-title="Magenta"></span></li>
									  		<li><span class="bg-color-pink" data-widget-setstyle="jarviswidget-color-pink" rel="tooltip" data-placement="right" data-original-title="Pink"></span></li>
									  		<li><span class="bg-color-pinkDark" data-widget-setstyle="jarviswidget-color-pinkDark" rel="tooltip" data-placement="left" data-original-title="Fade Pink"></span></li>
									  		<li><span class="bg-color-blueLight" data-widget-setstyle="jarviswidget-color-blueLight" rel="tooltip" data-placement="top" data-original-title="Light Blue"></span></li>
									  		<li><span class="bg-color-teal" data-widget-setstyle="jarviswidget-color-teal" rel="tooltip" data-placement="top" data-original-title="Teal"></span></li>
									  		<li><span class="bg-color-blue" data-widget-setstyle="jarviswidget-color-blue" rel="tooltip" data-placement="top" data-original-title="Ocean Blue"></span></li>
									  		<li><span class="bg-color-blueDark" data-widget-setstyle="jarviswidget-color-blueDark" rel="tooltip" data-placement="top" data-original-title="Night Sky"></span></li>
									  		<li><span class="bg-color-darken" data-widget-setstyle="jarviswidget-color-darken" rel="tooltip" data-placement="right" data-original-title="Night"></span></li>
									  		<li><span class="bg-color-yellow" data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip" data-placement="left" data-original-title="Day Light"></span></li>
									  		<li><span class="bg-color-orange" data-widget-setstyle="jarviswidget-color-orange" rel="tooltip" data-placement="bottom" data-original-title="Orange"></span></li>
									  		<li><span class="bg-color-orangeDark" data-widget-setstyle="jarviswidget-color-orangeDark" rel="tooltip" data-placement="bottom" data-original-title="Dark Orange"></span></li>
									  		<li><span class="bg-color-red" data-widget-setstyle="jarviswidget-color-red" rel="tooltip" data-placement="bottom" data-original-title="Red Rose"></span></li>
									  		<li><span class="bg-color-redLight" data-widget-setstyle="jarviswidget-color-redLight" rel="tooltip" data-placement="bottom" data-original-title="Light Red"></span></li>
									  		<li><span class="bg-color-white" data-widget-setstyle="jarviswidget-color-white" rel="tooltip" data-placement="right" data-original-title="Purity"></span></li>
									  		<li><a href="javascript:void(0);" class="jarviswidget-remove-colors" data-widget-setstyle="" rel="tooltip" data-placement="bottom" data-original-title="Reset widget color to default">Remove</a></li>
									  	</ul>
									  	</div>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Column Filters </h2>
				
								<span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
				
								<!-- widget div-->
								<div role="content">
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<div id="datatable_fixed_column_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
											<table id="datatable_fixed_column" class="table table-striped table-bordered dataTable no-footer" width="100%" role="grid" aria-describedby="datatable_fixed_column_info" style="width: 100%;">
										        <thead>
												<tr role="row">
													<th class="hasinput" style="width:10%" rowspan="1" colspan="1">
														<input type="text" class="form-control" placeholder="id">
													</th>
													<th class="hasinput" style="width:10%" rowspan="1" colspan="1">
														<input type="text" class="form-control" placeholder="no">
													</th>
													<th class="hasinput" style="width:10%" rowspan="1" colspan="1">
														<input type="text" class="form-control" placeholder="이름">
													</th>
													<th class="hasinput" style="width:10%" rowspan="1" colspan="1">
														<input type="text" class="form-control" placeholder="사원번호">
													</th>
													<th class="hasinput" style="width:10%" rowspan="1" colspan="1">
														<input type="text" class="form-control" placeholder="부서명">
													</th>
													<th class="hasinput" style="width:10%" rowspan="1" colspan="1">
														<input type="text" class="form-control" placeholder="직급">
													</th>
													<th class="hasinput" style="width:10%" rowspan="1" colspan="1">
														<input type="text" class="form-control" placeholder="직책">
													</th>
													<th class="hasinput" style="width:10%" rowspan="1" colspan="1">
														<input type="text" class="form-control" placeholder="권한">
													</th>
													<th class="hasinput icon-addon" rowspan="1" colspan="1">
														<input id="dateselect_filter" type="text" placeholder="입사일" class="form-control datepicker hasDatepicker" data-dateformat="yy/mm/dd">
														<label for="dateselect_filter" class="glyphicon glyphicon-calendar no-margin padding-top-15" rel="tooltip" title="" data-original-title="Filter Date"></label>
													</th>
												</tr>
									            <tr role="row">
									            		<th data-class="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 246px;">id</th>
									            		<th data-class="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 246px;">No.</th>
									            		<th data-class="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 246px;">이름</th>
									            		<th data-class="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 246px;">사원번호</th>
									            		<th data-class="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 246px;">부서명</th>
									            		<th data-class="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 246px;">직급</th>
									            		<th data-class="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending" style="width: 246px;">직책</th>
									            		<th data-hide="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 230px;">권한</th>
									            		<th data-hide="expand" class="expand sorting" tabindex="0" aria-controls="datatable_fixed_column" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 230px;">입사일</th>
									        </thead>   
										</table>
										</div>
				             
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
						</article>
					</div>
				</section>
				<!-- end widget grid -->

			</div>			
			
<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			$('a[href="/user/userList"]').parent().addClass("active");
			$('a[href="/user/userList"]').parent().parent().parent().addClass("open");
			
			pageSetUp();
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
			/* COLUMN FILTER  */
		    var otable = $('#datatable_fixed_column').DataTable({
			    	//"bFilter": false,
			    	//"bInfo": false,
			    	//"bLengthChange": false
			    	//"bAutoWidth": false,
			    	//"bPaginate": false,
			    	//"bStateSave": true // saves sort state using localStorage
			    	"order":[[0,"desc"]],
		    		"bProcessing": true,
		    		"sAjaxSource": '/user/selectUserList',
				"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6 hidden-xs'f><'col-sm-6 col-xs-12 hidden-xs'<'toolbar'>>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
				"autoWidth" : true,
				"oLanguage": {
					"sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
				},
				"columns" : [
					      	  {data: "id","defaultContent": ""},
					      	  {data: "rownum","defaultContent": ""},
					      	  {data: "emp_name","defaultContent": ""},
					          {data: "emp_number","defaultContent": ""},
					      	  {data: "emp_dept","defaultContent": ""},
					      	  {data: "emp_duty","defaultContent": ""},
					          {data: "emp_position","defaultContent": ""},
					      	  {data: "emp_permission","defaultContent": ""},
					          {data: "emp_join_date","defaultContent": ""}
					      ],
					      columnDefs : [
					          {
					              "targets": [1,2,3,4,5],
					              "visible": true,
					          },
					          {
					              "targets": 0,
					              "visible": false,
					          }
					      ],
		    });
		    // custom toolbar
		    $("div.toolbar").html('<div class="text-right"><img src="/smartAdmin/img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');
		    	   
		    // Apply the filter
		    $("#datatable_fixed_column thead th input[type=text]").on( 'keyup change', function () {
		    	
		        otable
		            .column( $(this).parent().index()+':visible' )
		            .search( this.value )
		            .draw();
		            
		    } );
		    /* END COLUMN FILTER */   
		     $('#datatable_fixed_column tbody').css('cursor', 'pointer');
		    $('#datatable_fixed_column tbody').on('click', 'tr', function () {
		        var selectedUserId = otable.row(this).data()['id'];
		        document.location.href="/user/userView?userId="+selectedUserId;
		    });
		    
		    $('a').bind('click', function(){
		    		switch(this.id){
		    			case "userWriteBtn": document.location.href="/user/userWrite";	break;
		    		}
		    });
		});

		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		

		
		</script>			