<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" isErrorPage="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="authz" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="sp" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ft"  uri="http://www.anynori.com/taglibs/tags" %>
<%@ taglib prefix="ff"  uri="http://www.anynori.com/taglibs/functions" %>
<div id="ribbon">

				<span class="ribbon-button-alignment"> 
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
						<i class="fa fa-refresh"></i>
					</span> 
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li>Home</li><li>Tables</li><li>Data Tables</li>
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
<div id="content">
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa-fw fa fa-home"></i> 공통정보관리 <span>&gt;
					직원등록</span>
			</h1>
		</div>
	</div>
	<section id="widget-grid" class="">
		<div class="row">
		
			<article
				class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
				
				<div
					class="jarviswidget jarviswidget-sortable jarviswidget-color-blueDark"
					id="wid-id-0" role="widget"
					data-widget-attstyle="jarviswidget-color-purple" style="">
					<header role="heading">
						<div class="jarviswidget-ctrls" role="menu">
							<a href="javascript:void(0);"
								class="button-icon jarviswidget-edit-btn" rel="tooltip" title=""
								data-placement="bottom" data-original-title="Edit"><i
								class="fa fa-cog "></i></a> <a href="javascript:void(0);"
								class="button-icon jarviswidget-toggle-btn" rel="tooltip"
								title="" data-placement="bottom" data-original-title="Collapse"><i
								class="fa fa-minus "></i></a> <a href="javascript:void(0);"
								class="button-icon jarviswidget-fullscreen-btn" rel="tooltip"
								title="" data-placement="bottom"
								data-original-title="Fullscreen"><i class="fa fa-expand "></i></a>
							<a href="javascript:void(0);"
								class="button-icon jarviswidget-delete-btn" rel="tooltip"
								title="" data-placement="bottom" data-original-title="Delete"><i
								class="fa fa-times"></i></a>
						</div>
						<div class="widget-toolbar" role="menu">
							<a data-toggle="dropdown"
								class="dropdown-toggle color-box selector"
								href="javascript:void(0);"></a>
							<ul
								class="dropdown-menu arrow-box-up-right color-select pull-right">
								<li><span class="bg-color-green"
									data-widget-setstyle="jarviswidget-color-green" rel="tooltip"
									data-placement="left" data-original-title="Green Grass"></span></li>
								<li><span class="bg-color-greenDark"
									data-widget-setstyle="jarviswidget-color-greenDark"
									rel="tooltip" data-placement="top"
									data-original-title="Dark Green"></span></li>
								<li><span class="bg-color-greenLight"
									data-widget-setstyle="jarviswidget-color-greenLight"
									rel="tooltip" data-placement="top"
									data-original-title="Light Green"></span></li>
								<li><span class="bg-color-purple"
									data-widget-setstyle="jarviswidget-color-purple" rel="tooltip"
									data-placement="top" data-original-title="Purple"></span></li>
								<li><span class="bg-color-magenta"
									data-widget-setstyle="jarviswidget-color-magenta" rel="tooltip"
									data-placement="top" data-original-title="Magenta"></span></li>
								<li><span class="bg-color-pink"
									data-widget-setstyle="jarviswidget-color-pink" rel="tooltip"
									data-placement="right" data-original-title="Pink"></span></li>
								<li><span class="bg-color-pinkDark"
									data-widget-setstyle="jarviswidget-color-pinkDark"
									rel="tooltip" data-placement="left"
									data-original-title="Fade Pink"></span></li>
								<li><span class="bg-color-blueLight"
									data-widget-setstyle="jarviswidget-color-blueLight"
									rel="tooltip" data-placement="top"
									data-original-title="Light Blue"></span></li>
								<li><span class="bg-color-teal"
									data-widget-setstyle="jarviswidget-color-teal" rel="tooltip"
									data-placement="top" data-original-title="Teal"></span></li>
								<li><span class="bg-color-blue"
									data-widget-setstyle="jarviswidget-color-blue" rel="tooltip"
									data-placement="top" data-original-title="Ocean Blue"></span></li>
								<li><span class="bg-color-blueDark"
									data-widget-setstyle="jarviswidget-color-blueDark"
									rel="tooltip" data-placement="top"
									data-original-title="Night Sky"></span></li>
								<li><span class="bg-color-darken"
									data-widget-setstyle="jarviswidget-color-darken" rel="tooltip"
									data-placement="right" data-original-title="Night"></span></li>
								<li><span class="bg-color-yellow"
									data-widget-setstyle="jarviswidget-color-yellow" rel="tooltip"
									data-placement="left" data-original-title="Day Light"></span></li>
								<li><span class="bg-color-orange"
									data-widget-setstyle="jarviswidget-color-orange" rel="tooltip"
									data-placement="bottom" data-original-title="Orange"></span></li>
								<li><span class="bg-color-orangeDark"
									data-widget-setstyle="jarviswidget-color-orangeDark"
									rel="tooltip" data-placement="bottom"
									data-original-title="Dark Orange"></span></li>
								<li><span class="bg-color-red"
									data-widget-setstyle="jarviswidget-color-red" rel="tooltip"
									data-placement="bottom" data-original-title="Red Rose"></span></li>
								<li><span class="bg-color-redLight"
									data-widget-setstyle="jarviswidget-color-redLight"
									rel="tooltip" data-placement="bottom"
									data-original-title="Light Red"></span></li>
								<li><span class="bg-color-white"
									data-widget-setstyle="jarviswidget-color-white" rel="tooltip"
									data-placement="right" data-original-title="Purity"></span></li>
								<li><a href="javascript:void(0);"
									class="jarviswidget-remove-colors" data-widget-setstyle=""
									rel="tooltip" data-placement="bottom"
									data-original-title="Reset widget color to default">Remove</a></li>
							</ul>
						</div>
						<span class="widget-icon"> <i class="fa fa-comments"></i>
						</span>
						<h2>Widget Title</h2>
						<span class="jarviswidget-loader"><i
							class="fa fa-refresh fa-spin"></i></span>
					</header>
					<div role="content">
						<div class="jarviswidget-editbox">
							<input class="form-control" type="text">
						</div>
						<div class="widget-body">
						<div class="col-sm-2 profile-pic" style="text-align:left;">
						<div>
							<span><a href="javascript:void(0);" class="btn btn-primary btn-sm">Edit</a> <a href="javascript:void(0);" class="btn btn-danger btn-sm">Delete</a></span>
							</div>
							<img src="/smartAdmin/img/superbox/superbox-full-1.jpg" alt="demo user" style="top:0px; max-width:210px; border: 0px solid #fff; margin-top:10px;">
						</div>
							
						<form class = "form-horizontal">
							<fieldset>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">사원번호</label>
									<div class="col-md-10">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">아이디</label>
									<div class="col-md-10">
										<input class="form-control" type="text" autocomplete="false">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">비밀번호</label>
									<div class="col-md-10">
										<input class="form-control" type="password" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">비밀번호 확인</label>
									<div class="col-md-10">
										<input class="form-control" type="password" value="">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">성명</label>
									<div class="col-md-10">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">직급</label>
									<div class="col-md-10">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">직책</label>
									<div class="col-md-10">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">부서명</label>
									<div class="col-md-10">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">종전부서</label>
									<div class="col-md-10">
										<input class="form-control" type="text">
									</div>
								</div>				
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">사용권한</label>
									<div class="col-md-10">
										<input class="form-control" type="text" list="list">
										<datalist id="list">
											<option value="0">0</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</datalist>
										<p class="note"><strong>Note:</strong> works in Chrome, Firefox, Opera and IE10.</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">연락처</label>
									<div class="col-md-3">
										<input class="form-control" type="text">
									</div>
									<div class="col-md-3">
										<input class="form-control" type="text">
									</div>
									<div class="col-md-3">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">이메일</label>
									<div class="col-md-4">
										<input class="form-control" type="text">
									</div>
									<div class="col-md-1">
										<p align ="center"> @ </p>
									</div>
									<div class="col-md-4">
										<input class="form-control" type="text" list="list">
										<datalist id="list">
											<option value="0">0</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
										</datalist>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">최종학력</label>
									<div class="col-md-10">
										<label class="radio radio-inline">				
											<input type="radio" class="radiobox" name="style-0a">
											<span>고졸이하</span>					
										</label>
										<label class="radio radio-inline">
											<input type="radio" class="radiobox" name="style-0a">
											<span>전문학사</span>  
										</label>			
										<label class="radio radio-inline">
											<input type="radio" class="radiobox" name="style-0a">
											<span>학사</span>  
										</label>
										<label class="radio radio-inline">
											<input type="radio" class="radiobox" name="style-0a">
											<span>석사</span>  
										</label>	
										<label class="radio radio-inline">
											<input type="radio" class="radiobox" name="style-0a">
											<span>박사</span>  
										</label>	
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">최종졸업학교</label>
									<div class="col-md-4">
										<label class="radio radio-inline">				
											<input type="radio" class="radiobox" name="style-0a">
											<span>검정고시</span>					
										</label>
										<label class="radio radio-inline">
											<input type="radio" class="radiobox" name="style-0a">
											<span>학교명</span>  
										</label>					
									</div>
									<div class="col-md-6">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">자격증</label>
									<div class="col-md-10">
										<textarea class="form-control" rows="4"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">생년월일</label>
									<div class="col-md-10">
										<input type="text" name="birth_date" id="birth_date" class="form-control" data-dateformat="yyyy/mm/dd" id="dp1574898628023">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">입사일</label>
									<div class="col-md-10">
										<input type="text" name="join_date" id="join_date" class="form-control" data-dateformat="yyyy/mm/dd" id="dp1574898628023">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">퇴사일</label>
									<div class="col-md-10">
										<input type="text" name="leave_date" id="leave_date" class="form-control" data-dateformat="yyyy/mm/dd" id="dp1574898628023">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">보직변경일</label>
									<div class="col-md-10">
										<input type="text" name="mydate" class="form-control datepicker hasDatepicker" data-dateformat="dd/mm/yy" id="dp1574901738645">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">휴직일</label>
									<div class="col-md-5">
										<input type="text" name="rest_startdate" id="rest_startdate" class="form-control" data-dateformat="yyyy/mm/dd" id="dp1574898628023">
										
									</div>
									<div class="col-md-5">
										<input type="text" name="rest_finishdate" id="rest_finishdate" class="form-control" data-dateformat="yyyy/mm/dd" id="dp1574898628023">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">복직일</label>
									<div class="col-md-10">
										<input type="text" name="rejoin_date" id="rejoin_date" class="form-control" data-dateformat="yyyy/mm/dd" id="dp1574898628023">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">우편번호</label>
									<div class="col-md-7">
										<input class="form-control" type="text">
									</div>
									<div class="col-md-3">
										<a class="btn btn-primary" href="javascript:void(0);">우편번호검색</a>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">주소</label>
									<div class="col-md-10">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">상세주소</label>
									<div class="col-md-10">
										<input class="form-control" type="text">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-1 control-label"class="col-md-1 control-label" style="text-align:left;">특기사항</label>
									<div class="col-md-10">
										<textarea class="form-control" rows="4"></textarea>
									</div>
								</div>		
							</fieldset>
							</form>
						</div>
					</div>
				</div>
			</article>
		</div>
		<div class="row">
			<div class="col-sm-12"></div>
		</div>
	</section>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('#birth_date').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		
	});
	$('#join_date').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		
	});
	$('#leave_date').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		
	});
	$('#rest_startdate').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		onSelect: function (selectedDate) {
			$('#finishdate').datepicker('option', 'minDate', selectedDate);
		}
	});
	$('#rest_finishdate').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		onSelect: function (selectedDate) {
			$('#startdate').datepicker('option', 'maxDate', selectedDate);
		}
	});
	$('#rejoin_date').datepicker({
		dateFormat: 'yy.mm.dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		
	});
			
		});
			
</script>
