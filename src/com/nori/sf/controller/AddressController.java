package com.nori.sf.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
@RequestMapping(value = "/addr")
public class AddressController {
	
	
	@RequestMapping(value = "/jusoPopup")
	public String jusoPopup(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return "addr/jusoPopup";
	}
	@RequestMapping(value = "/jusoPopup2")
	public String jusoPopup2(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return "addr/jusoPopup2";
	}
	
}