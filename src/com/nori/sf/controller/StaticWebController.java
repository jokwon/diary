package com.nori.sf.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.NestedServletException;

import com.nori.Core.utility.HttpUtil;
import com.nori.Core.view.JsonResponse;

@Controller
public class StaticWebController{
	
	static {
		org.apache.ibatis.logging.LogFactory.useLog4JLogging();
	}
	

	@RequestMapping(value = "/")
	public String index(Authentication authentication, Principal principal) throws Exception {
		if (authentication == null) { 
			return "login";
		}
		return "forward:dashBoard";
	}
	
	@RequestMapping(value = "/dashBoard")
	public String indexPage(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return "dashBoard";
	}
	
	
	@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
	@RequestMapping(value = "/authfail")
	public ModelAndView authfail(HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setHeader("Cache-control", "no-cache");
		if (HttpUtil.isAjaxRequest(request, response)) {
			JsonResponse jsonResponse = new JsonResponse();

			jsonResponse.setStatus(HttpStatus.UNAUTHORIZED);

			jsonResponse.setSuccess(false);

			jsonResponse.setMessage("Login Required!");

			return new ModelAndView("jsonView", "jsonResponse", jsonResponse);
		} else {
			return new ModelAndView("login");
		}
	}

	@ResponseStatus(value = HttpStatus.NOT_ACCEPTABLE)
	@RequestMapping(value = "/denied")
	public ModelAndView denied(Authentication authentication, HttpServletRequest request, HttpServletResponse response) throws Exception {
		response.setHeader("Cache-control", "no-cache");

		if (HttpUtil.isAjaxRequest(request, response)) {
			JsonResponse jsonResponse = new JsonResponse();

			jsonResponse.setStatus(HttpStatus.NOT_ACCEPTABLE);

			jsonResponse.setSuccess(false);
			
			jsonResponse.setMessage("Access Denied!");

			return new ModelAndView("jsonView", "jsonResponse", jsonResponse);
		} else {
			return new ModelAndView("denied");
		}
	}

	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@RequestMapping(value = "/page404")
	public ModelAndView handle404(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Cache-control", "no-cache");
		if (HttpUtil.isAjaxRequest(request, response)) {
			JsonResponse jsonResponse = new JsonResponse();

			jsonResponse.setStatus(HttpStatus.NOT_FOUND);

			jsonResponse.setSuccess(false);

			jsonResponse.setMessage("Page Not Found!");

			return new ModelAndView("jsonView", "jsonResponse", jsonResponse);
		} else {
			return new ModelAndView("page404");
		}
	}

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@RequestMapping(value = "/page500")
	public ModelAndView handle500(HttpServletRequest request, HttpServletResponse response, Exception ex) {
		response.setHeader("Cache-control", "no-cache");
		
		StringBuilder output = new StringBuilder();
		if (request.getAttribute("javax.servlet.error.exception") != null) {
			Throwable exx = (Throwable) request.getAttribute("javax.servlet.error.exception");
			if (exx instanceof NestedServletException) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				exx.getCause().printStackTrace(pw);
				pw.close();
				output.append(sw.toString());
			} else {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				exx.printStackTrace(pw);
				pw.close();
				output.append(sw.toString());
			}
		} else if (ex instanceof NestedServletException) {
			if (ex.getCause() != null && ex.getCause().getMessage() != null) {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				ex.getCause().printStackTrace(pw);
				pw.close();
				output.append(sw.toString());
			} else {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				ex.printStackTrace(pw);
				pw.close();
				output.append(sw.toString());
			}
		} else {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			pw.close();
			output.append(sw.toString());
		}
		
		if (HttpUtil.isAjaxRequest(request, response)) {
			JsonResponse jsonResponse = new JsonResponse();

			jsonResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);

			jsonResponse.setSuccess(false);

			jsonResponse.setMessage(output.toString());
			return new ModelAndView("jsonView", "jsonResponse", jsonResponse);
		} else {
			Throwable exx = (Throwable) request.getAttribute("javax.servlet.error.exception");
			String message = (exx.getCause() == null ? exx.toString() : exx.getCause().toString());
			if (message.indexOf("\n") != -1) {
				message = message.substring(0, message.indexOf("\n"));
			}
			ModelAndView view = new ModelAndView("page500");
			view.addObject("message", message);
			view.addObject("output", output.toString());
			return view;
		}
	}
}