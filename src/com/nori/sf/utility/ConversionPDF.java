package com.nori.sf.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.CssAppliers;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;

public class ConversionPDF {

	//local 서버
	private static final String uploadpath	="/upload";
	private static final String fileUploadPath = "/Users/kwonjo/Desktop/primeWorkspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/upload";
	private String dailyRecordPDF ="dailyRecord.pdf";
	private static final String pdfSavingPath ="/savingPDF";
	private static final String resourcePath ="/Users/kwonjo/Desktop/primeWorkspace/prime/web";
	//cafe24 서버
	private static final String sUploadpath	="/upload";
	private static final String sFileUploadPath = "/home/hosting_users/vkvlood/tomcat/webapps/upload";
	private String sDailyRecordPDF ="dailyRecord.pdf";
	private static final String sPdfSavingPath ="/savingPDF";
	private static final String sResourcePath ="/home/hosting_users/vkvlood/tomcat/webapps/ROOT";
	
	//noriter 서버
	private static final String nUploadpath	="/upload";
	private static final String nFileUploadPath = "/usr/local/tomcat8/webapps/upload";
	private String nDailyRecordPDF ="dailyRecord.pdf";
	private static final String nPdfSavingPath ="/savingPDF";
	private static final String nResourcePath ="/usr/local/tomcat8/webapps/ROOT";
	
    ///Users/kwonjo/Desktop/primeWorkspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/upload/savingPDF/dailyRecord.pdf
    public String createPdf(String dailyRecord, HttpServletResponse response)
    throws DocumentException, IOException {
    	
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
        Calendar c1 = Calendar.getInstance();
        String strToday  = sdf.format(c1.getTime());
        
        //fileName = 현재날짜(시분초) + 방문아이디 + 디폴트네임
        
        //local
        //dailyRecordPDF	=	File.separator+strToday + visiteId + dailyRecordPDF;
        
        //noriter
        nDailyRecordPDF	=	File.separator+strToday + dailyRecordPDF;
        
        int idx = dailyRecordPDF.indexOf(File.separator); 
        
        //local
        //String fileNm		=	 uploadpath+pdfSavingPath+File.separator+dailyRecordPDF.substring(idx+1);
        
        //noriter
        String fileNm		=	 nUploadpath+nPdfSavingPath+File.separator+nDailyRecordPDF;
        
        
        //local
        //String serverUploadPath = fileUploadPath+pdfSavingPath+dailyRecordPDF;
        
        	//noriter
        	String serverUploadPath = nFileUploadPath+nPdfSavingPath+File.separator+nDailyRecordPDF;
        
        Document document = new Document(PageSize.A4, 20, 20, 30, 30);     
        XMLWorkerHelper xmlWorkerHelper = XMLWorkerHelper.getInstance();
        PdfWriter writer	= PdfWriter.getInstance(document, new FileOutputStream(serverUploadPath));
        writer.setInitialLeading(12.5f);
        document.open();
//CSS FILE
        CSSResolver cssResolver = new StyleAttrCSSResolver();
        CssFile cssFile1 = xmlWorkerHelper.getCSS(new FileInputStream(nResourcePath + "/coreui/vendors/@coreui/icons/css/coreui-icons.min.css"));
        CssFile cssFile2 = xmlWorkerHelper.getCSS(new FileInputStream(nResourcePath + "/coreui/vendors/flag-icon-css/css/flag-icon.min.css"));
        CssFile cssFile3 = xmlWorkerHelper.getCSS(new FileInputStream(nResourcePath + "/coreui/vendors/font-awesome/css/font-awesome.min.css"));
        CssFile cssFile4 = xmlWorkerHelper.getCSS(new FileInputStream(nResourcePath + "/coreui/vendors/simple-line-icons/css/simple-line-icons.css"));
        CssFile cssFile5 = xmlWorkerHelper.getCSS(new FileInputStream(nResourcePath + "/coreui/css/style.css"));
        CssFile cssFile6 = xmlWorkerHelper.getCSS(new FileInputStream(nResourcePath + "/coreui/vendors/pace-progress/css/pace.min.css"));
        CssFile cssFile7 = xmlWorkerHelper.getCSS(new FileInputStream(nResourcePath + "/coreui/vendors/datatables.net-bs4/css/dataTables.bootstrap4.css"));
        cssResolver.addCss(cssFile1);
        cssResolver.addCss(cssFile2);
        cssResolver.addCss(cssFile3);
        cssResolver.addCss(cssFile4);
        cssResolver.addCss(cssFile5);
        cssResolver.addCss(cssFile6);
        cssResolver.addCss(cssFile7);
        
        
     // HTML, 폰트 설정
        XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
        fontProvider.register(nResourcePath+"/BM-Dohyeon-Regular.ttf", "Dohyeon"); // MalgunGothic은 alias,
        CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
         
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
        
        PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
        
        XMLWorker worker = new XMLWorker(css, true);
        XMLParser xmlParser = new XMLParser(worker, Charset.forName("UTF-8"));
        
        StringReader strReader = new StringReader(dailyRecord);
        System.out.println(strReader);
        
        
        
        xmlParser.parse(strReader);
         
        document.close();
        writer.close();
        
        //downloadDailyRecordPDF(response, serverUploadPath, fileNm);
        return fileNm;
        
    }
    
    public void downloadDailyRecordPDF(HttpServletResponse response, String serverUploadPath, String fileNm) {
    	FileInputStream fis = null;
    	ServletOutputStream out = null;

    		try {
    			out = response.getOutputStream();
    			
    			//String serverUploadPath = "/Users/jungminhwang/prime_workspace/prime/upload"+File.separator + contract.getContractDbFile();
    			//
    	
    			File file = new File(serverUploadPath);
    	
    			if (!file.exists()) {
    				String str = "<script>alert('저장된 파일이 없습니다.'); history.back(-1);</script>";
    				response.setContentType("text/html; charset=UTF-8");
    				response.getOutputStream().write(str.getBytes());
    				return;
    			}
    	
    			fis = new FileInputStream(file);
    			System.out.println(fileNm);
    			response.setHeader("Content-Type", "application/octet-stream; charset=utf-8");
    			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNm + "\";");
    			response.setHeader("Content-Transfer-Encoding", "binary;");
    	
    			response.setHeader("Pragma", "no-cache;");
    			response.setHeader("Expires", "-1;");
    	
    			int byteRead = 0;
    			byte[] buffer = new byte[8192];
    			while ((byteRead = fis.read(buffer, 0, 8192)) != -1) {
    				out.write(buffer, 0, byteRead);
    			}
    			System.out.println("sdfsds");
    			out.flush();
    		} catch (Exception e) {
    			// TODO: handle exception
    			throw new RuntimeException(e);
    		} finally {
    			if (out != null)
    				try {
    					out.close();
    				} catch (IOException e) {
    				}
    			if (fis != null)
    				try {
    					fis.close();
    				} catch (IOException e) {
    				}
    		}
    }
	
	
	
}
