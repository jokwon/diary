package com.nori.sf.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import com.nori.Core.tx.RequiredTx;
import com.nori.Core.utility.CookieUtil;
import com.nori.sf.service.UserAccessLogService;

public class SimpleUrlAuthenticationSuccessHandler extends org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler {

	@Autowired
	private UserAccessLogService userAccessLogService;
	
	public SimpleUrlAuthenticationSuccessHandler() {
		super();
	}

	public SimpleUrlAuthenticationSuccessHandler(String defaultTargetUrl) {
		super(defaultTargetUrl);
	}

	@Override
	@RequiredTx
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		if ("true".equalsIgnoreCase(request.getParameter("remember_me"))) {
			CookieUtil.setCookie(request, response, "userId", request.getParameter("userId"), "", 60 * 60 * 24 * 15);
		}
		CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
		if (userDetails != null && userDetails.getUser() != null) {
			this.userAccessLogService.insert(userDetails.getUser(), request);
		}

		super.onAuthenticationSuccess(request, response, authentication);
	}
}
