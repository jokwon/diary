package com.nori.sf.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import com.nori.Core.domain.User;
import com.nori.Core.tx.RequiredTx;
import com.nori.sf.service.UserAccessLogService;

public class LogoutSuccessHandler
		extends org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler {

	@Autowired
	private UserAccessLogService userAccessLogService;

	@RequiredTx
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {

		if (authentication != null) {
			CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
			if (userDetails != null && userDetails.getUser() != null) {
				User user = userDetails.getUser();
				if (user.getUserAccessLog() != null) {
					this.userAccessLogService.updateLogoutManualLog(user.getUserAccessLog());
				}
			}
		}
		super.onLogoutSuccess(request, response, authentication);
	}
}
