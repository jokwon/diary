package com.nori.sf.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.nori.Core.domain.User;
import com.nori.Core.domain.UserRoles;
import com.nori.Core.security.SecurityGrantedAuthority;
import com.nori.sf.service.UserService;

@Component("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	private static Logger logger = Logger.getLogger(UserDetailsService.class);

	@Autowired
	private UserService userService;

	private List<String> allowedRoleCodes;

	@Override
	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		User user = this.userService.selectUserForLogin(userId);
		if (user != null) {
			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			List<UserRoles> userRoleList = user.getUserRoleList();
			logger.debug("allowedRoleCodes : " + allowedRoleCodes);

			boolean denied = true;

			if ((userRoleList != null) && (!userRoleList.isEmpty())) {
				for (UserRoles userRoles : userRoleList) {
					authorities.add(SecurityGrantedAuthority.getRole(userRoles.getRoleCode()));
					logger.debug("user roleCode : " + userRoles.getRoleCode());

					if (allowedRoleCodes.contains(userRoles.getRoleCode())) {
						denied = false;
					}
				}
			}

			if (denied) {
				throw new PermissionDeniedException("no role for user.");
			}

			/**
			 * CustomUserDetails(ID,PASSWD,계정사용여부,계정만료여부,계정패스워드만료여부,계정잠김여부,권한들);
			 */
			logger.trace(user.toString());
			CustomUserDetails customUser = new CustomUserDetails(user.getUserId(), user.getPassword(), true, true, true, true, authorities);
			customUser.setUser(user);

			return customUser;
		}

		throw new UsernameNotFoundException("user not found");
	}

	public List<String> getAllowedRoleCodes() {
		return allowedRoleCodes;
	}

	public void setAllowedRoleCodes(List<String> allowedRoleCodes) {
		this.allowedRoleCodes = allowedRoleCodes;
	}

}
