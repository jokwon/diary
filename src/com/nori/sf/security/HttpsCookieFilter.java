package com.nori.sf.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class HttpsCookieFilter implements Filter {

	private static Logger logger = Logger.getLogger(HttpsCookieFilter.class);
	private String sessionId = "PMS_JSESSIONID";

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		if (logger.isTraceEnabled()) {
			logger.trace("[HttpsCookieFilter] initialized.");
		}
	}

	@Override
	public void destroy() {
		if (logger.isTraceEnabled()) {
			logger.trace("[HttpsCookieFilter] destoryed.");
		}
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		final HttpSession session = httpRequest.getSession(false);

		if (session != null) {
			final Cookie sessionCookie = new Cookie(sessionId, session.getId());

			sessionCookie.setMaxAge(-1);
			sessionCookie.setSecure(false);
			sessionCookie.setPath(httpRequest.getContextPath());

			httpResponse.addCookie(sessionCookie);
		}
		chain.doFilter(request, response);
	}

}
