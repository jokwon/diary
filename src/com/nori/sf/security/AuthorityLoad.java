package com.nori.sf.security;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.nori.Core.security.Role;
import com.nori.Core.security.SecurityGrantedAuthority;
import com.nori.sf.service.UserRoleService;

@Component
public class AuthorityLoad implements InitializingBean {

	@Autowired
	private UserRoleService userRoleService;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		/*인터페이스 호출*/		
		if (Role.USER.equals(Role.USER)) {
		}
		try {
			List<GrantedAuthority> authorities = SecurityGrantedAuthority.getRoles();
			
			for (GrantedAuthority grantedAuthority : authorities) {
				SecurityGrantedAuthority role = (SecurityGrantedAuthority) grantedAuthority;
				
			}
			
			this.userRoleService.insertRole(authorities);
		} catch (Exception ex) {
		}
		
	}

}
