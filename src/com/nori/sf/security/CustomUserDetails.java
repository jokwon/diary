package com.nori.sf.security;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;

public class CustomUserDetails extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = 1L;
	
	private com.nori.Core.domain.User user;
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString()).append(": ");
		sb.append("Username: ").append(super.getUsername()).append("; ");
		sb.append("Password: [PROTECTED]; ");
		sb.append("Enabled: ").append(super.isEnabled()).append("; ");
		sb.append("AccountNonExpired: ").append(super.isAccountNonExpired()).append("; ");
		sb.append("credentialsNonExpired: ").append(super.isCredentialsNonExpired()).append("; ");
		sb.append("AccountNonLocked: ").append(super.isAccountNonLocked()).append("; ");

		if (!super.getAuthorities().isEmpty()) {
			sb.append("Granted Authorities: ");

			boolean first = true;
			for (GrantedAuthority auth : super.getAuthorities()) {
				if (!first) {
					sb.append(",");
				}
				first = false;

				sb.append(auth);
			}
		} else {
			sb.append("Not granted any authorities");
		}

		return sb.toString();
	}

	public CustomUserDetails(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public com.nori.Core.domain.User getUser() {
		return user;
	}

	public void setUser(com.nori.Core.domain.User user) {
		this.user = user;
	}

}
