package com.nori.sf.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;

import com.nori.Core.utility.HttpUtil;

public class LoginUrlAuthenticationEntryPoint extends org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint {

	private String ajaxLoginFormUrl;

	public LoginUrlAuthenticationEntryPoint(String loginFormUrl) {
		super(loginFormUrl);
	}

	@Override
	protected String determineUrlToUseForThisRequest(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
		if (HttpUtil.isAjaxRequest(request, response)) {
			return getAjaxLoginFormUrl();
		}

		return getLoginFormUrl();
	}

	public String getAjaxLoginFormUrl() {
		return ajaxLoginFormUrl;
	}

	public void setAjaxLoginFormUrl(String ajaxLoginFormUrl) {
		this.ajaxLoginFormUrl = ajaxLoginFormUrl;
	}
	

}
