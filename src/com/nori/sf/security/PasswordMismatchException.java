package com.nori.sf.security;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class PasswordMismatchException extends UsernameNotFoundException {

	private static final long serialVersionUID = 1L;

	public PasswordMismatchException(String msg) {
		super(msg);
	}

	public PasswordMismatchException(String msg, Throwable t) {
		super(msg, t);
	}
}
