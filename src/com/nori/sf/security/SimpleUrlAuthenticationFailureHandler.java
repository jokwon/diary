package com.nori.sf.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;

import com.nori.Core.tx.ReadTx;
import com.nori.sf.service.UserLoginFailLogService;

public class SimpleUrlAuthenticationFailureHandler
		extends org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler {

	private static Logger logger = Logger.getLogger(SimpleUrlAuthenticationFailureHandler.class);
	
	@Autowired
	private UserLoginFailLogService userLoginFailLogService;
	
	private String usernameParameter = "userId";
	
	private String passwordParameter = "userPassword";

	public SimpleUrlAuthenticationFailureHandler() {
		super();
	}

	public SimpleUrlAuthenticationFailureHandler(String defaultFailureUrl) {
		super(defaultFailureUrl);
	}

	@Override
	@ReadTx
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		String id = "";
		try {
			id = request.getParameter(getUsernameParameter());
			this.userLoginFailLogService.insert(id, request);
		} catch (Exception ex) {
		
		}
		logger.debug(exception);
		
		if( exception != null  && exception.getCause() != null && exception.getCause() instanceof PermissionDeniedException ){
			setDefaultFailureUrl("/?login_error=denied");
		}else{
			setDefaultFailureUrl("/?login_error=failure");
		}
		
		super.onAuthenticationFailure(request, response, exception);
	}

	public String getUsernameParameter() {
		return usernameParameter;
	}

	public void setUsernameParameter(String usernameParameter) {
		this.usernameParameter = usernameParameter;
	}

	public String getPasswordParameter() {
		return passwordParameter;
	}

	public void setPasswordParameter(String passwordParameter) {
		this.passwordParameter = passwordParameter;
	}

}
