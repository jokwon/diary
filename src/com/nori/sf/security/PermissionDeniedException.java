package com.nori.sf.security;

public class PermissionDeniedException extends RuntimeException {

	private static final long serialVersionUID = -1540822197497333033L;

	public PermissionDeniedException(String msg) {
		super(msg);
	}
	
}
