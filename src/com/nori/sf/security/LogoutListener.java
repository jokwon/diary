package com.nori.sf.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import com.nori.Core.domain.User;
import com.nori.sf.service.UserAccessLogService;

@Component
public class LogoutListener implements ApplicationListener<SessionDestroyedEvent> {

	@Autowired
	private UserAccessLogService userAccessLogService;

	@Override
	public void onApplicationEvent(SessionDestroyedEvent event) {

		List<SecurityContext> listSecurityContext = event.getSecurityContexts();
		if (listSecurityContext != null) {
			for (SecurityContext securityContext : listSecurityContext) {
				if (securityContext != null && securityContext.getAuthentication() != null
						&& securityContext.getAuthentication().getPrincipal() != null) {
					CustomUserDetails userDetails = (CustomUserDetails) securityContext.getAuthentication()
							.getPrincipal();
					if (userDetails != null && userDetails.getUser() != null) {
						User user = userDetails.getUser();
						if (user.getUserAccessLog() != null) {
							this.userAccessLogService.updateLogout(user.getUserAccessLog());
						}
					}
				}
			}
		}
	}
}
