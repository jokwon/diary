package com.nori.sf.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import com.nori.Core.tx.RequiredTx;
import com.nori.sf.service.UserAccessLogService;

public class SavedRequestAwareAuthenticationSuccessHandler extends org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler {

	@Autowired
	private UserAccessLogService userAccessLogService;
	
	public SavedRequestAwareAuthenticationSuccessHandler() {
		super();
	}

	@Override
	@RequiredTx
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {

		CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
		if (userDetails != null && userDetails.getUser() != null) {
			this.userAccessLogService.insert(userDetails.getUser(), request);
		}

		super.onAuthenticationSuccess(request, response, authentication);
	}
}