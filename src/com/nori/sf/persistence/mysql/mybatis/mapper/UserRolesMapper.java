package com.nori.sf.persistence.mysql.mybatis.mapper;

import java.util.List;

import com.nori.Core.domain.UserRoles;

public interface UserRolesMapper {

	List<UserRoles> selectUserRolesList(String userId);
	
	UserRoles selectUserRoles(String userId);
	
	int insert(UserRoles userRoles);
	
	int updateRoles(UserRoles userRoles);
	
	int delete(UserRoles userRoles);
}
