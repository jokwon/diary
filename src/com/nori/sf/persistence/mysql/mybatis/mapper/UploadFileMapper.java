package com.nori.sf.persistence.mysql.mybatis.mapper;

import com.nori.Core.domain.FileUpload;

public interface UploadFileMapper {
	
	int insertUploadFile(FileUpload fileUpload);
	
	FileUpload selectOneUploadFile(FileUpload fileUpload);
}
