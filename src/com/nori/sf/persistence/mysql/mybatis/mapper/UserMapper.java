package com.nori.sf.persistence.mysql.mybatis.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.nori.Core.domain.MailCheckToken;
import com.nori.Core.domain.User;

public interface UserMapper {
	
	User selectUserForLogin(String userId);
	
	int insertUserInfo(User user);
	
	int insertToken(MailCheckToken token);
	
	MailCheckToken selectToken(@Param("userId") String  userId, @Param("token") String token);
	
	int updateJoinStatus(@Param("userId") String  userId, @Param("joinStatus") String joinStatus);
	
	User selectUser(User user);
	
	List<User> selectListUser(User user);
	
	List<User> selectUserList(User user);

	List<User> selectListUserForSchedule(User user);
	
	int selectUserListCnt(User user);
	
	
	int insertUser(User user);
	
	User selectOneUser(int id);
	
	User selectUserEmail(String email);
	
	int updateUser(User user);
	
	int deleteUser(User user);
}
