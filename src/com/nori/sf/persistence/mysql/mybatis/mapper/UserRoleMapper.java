package com.nori.sf.persistence.mysql.mybatis.mapper;

import java.util.List;

import com.nori.Core.domain.UserRole;

public interface UserRoleMapper {
	
	List<UserRole> selectUserRoleList();
	
	public int insert(UserRole role);
	
	int update(UserRole role);
	
	int delete(UserRole role);
	
}
