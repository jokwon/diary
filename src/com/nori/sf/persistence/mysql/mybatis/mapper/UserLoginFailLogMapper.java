package com.nori.sf.persistence.mysql.mybatis.mapper;

import java.util.List;

import com.nori.Core.domain.UserLoginFailLog;

public interface UserLoginFailLogMapper {
	
	List<UserLoginFailLog> select();
	
	long count();
	
	int insert(UserLoginFailLog userLoginFailLog);
	
	int delete(UserLoginFailLog userLoginFailLog);

}
