package com.nori.sf.persistence.mysql.mybatis.mapper;

import java.util.List;

import com.nori.Core.domain.UserAccessLog;

public interface UserAccessLogMapper {

	List<UserAccessLog> select();

	long count();

	int insert(UserAccessLog userAccessLog);

	int updateLogout(UserAccessLog userAccessLog);

	int updateLogoutManualLog(UserAccessLog userAccessLog);

	int delete(UserAccessLog userAccessLog);

	int deleteByUserId(String userId);

}
