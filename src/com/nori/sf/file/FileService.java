package com.nori.sf.file;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.nori.Core.domain.FileUpload;



@Service
public class FileService {
	
	private String path	=	"/Users/kwonjo/Desktop/primeWorkspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/upload";
	private String dbUploadPath	=	"/upload/";
	
	
	
	
	
	private String pattenDate = new String("dd");
	private String pattenFile = new String("yyyyMMddHHmmss");

	/**
	 * 패스 생성(폴더)
	 * 
	 * @param path
	 */
	private void makePath(String path) {
		File savePath = new File(path);
		if (!savePath.exists())
			savePath.mkdirs();
	}

	private String encodeFileName(String filename) {
		try {
			return URLEncoder.encode(filename, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 단일 파일 업로드
	 * 
	 * @param multipartFile
	 * @param httpSession
	 * @return
	 */
	public FileUpload managerUploadFile(MultipartFile multipartFile, HttpSession httpSession) {
		FileUpload fileUpload = new FileUpload();
		
		SimpleDateFormat sfPathName = new SimpleDateFormat(pattenDate);
		SimpleDateFormat sfFileName = new SimpleDateFormat(pattenFile);
		
		// 업로드 파일이 존재하면
		if (multipartFile != null && !(multipartFile.getOriginalFilename().equals(""))) {
			// 파일크기 제한 (5MB)
			long filesize = multipartFile.getSize(); // 파일크기
			long limitFileSize = 20 * 1024 * 1024; // 5MB
			
			// 제한보다 파일크기가 클 경우
			if (limitFileSize < filesize) {
				return fileUpload;
			}
			//httpSession.getServletContext().getRealPath("/");
			// 저장경로
			
			String defaultPath =  path;// 서버기본경로																
			String uriPath = dbUploadPath +"managerBizDocFile" + File.separator + sfPathName.format(new Date()); //유동적인 경로(db저장경로)
			
			
			String uploadPath = "managerBizDocFile" + File.separator + sfPathName.format(new Date());
			//String path = defaultPath +"/"+ uploadPath; //기본 + 유동
			String path = defaultPath +"/"+ uploadPath; //기본 + 유동
			//String uriPath = "/Users/kwonjo/git/eduline/web/upload";
			
			// 저장경로 처리
			makePath(path);
			
			// 파일 저장명 처리 (20150702091941)
			String newFileName = sfFileName.format(new Date()) + "_" + ((int) (Math.random() * 10000)); // 새로
			// 파일이름
			String orgFileName = multipartFile.getOriginalFilename(); // 실제 파일이름
			String ext = orgFileName.substring(orgFileName.lastIndexOf('.') + 1);
			long fsize = multipartFile.getSize();
			
			//fileUpload.setPath(uriPath+"/"+newFileName+"."+ext);
			fileUpload.setDbUploadPath(uriPath);
			fileUpload.setExt(ext);
			fileUpload.setNewName(newFileName);
			fileUpload.setOrgName(orgFileName);
			fileUpload.setSize(fsize);
			// Multipart 처리
			try {
				// 서버에 파일 저장 (쓰기)
				multipartFile.transferTo(new File(path + File.separator + newFileName +"."+ ext));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		return fileUpload;
	}
	
//	public News uploadNewsFile(MultipartFile multipartFile, HttpSession httpSession) {
//		News news = new News();
//		
//		SimpleDateFormat sfPathName = new SimpleDateFormat(pattenDate);
//		SimpleDateFormat sfFileName = new SimpleDateFormat(pattenFile);
//		
//		
//		logger.debug("sfPathName ===> " + sfPathName.format(new Date()));
//		logger.debug("sfFileName ===> " + sfFileName.format(new Date()));
//		
//		// 업로드 파일이 존재하면
//		if (multipartFile != null && !(multipartFile.getOriginalFilename().equals(""))) {
//			// 파일크기 제한 (5MB)
//			long filesize = multipartFile.getSize(); // 파일크기
//			long limitFileSize = 20 * 1024 * 1024; // 5MB
//			
//			// 제한보다 파일크기가 클 경우
//			if (limitFileSize < filesize) {
//				return news;
//			}
//			//httpSession.getServletContext().getRealPath("/");
//			// 저장경로
//			
//			String defaultPath =  path;// 서버기본경로																
//			String uriPath = dbUploadPath +"newsPhoto" + File.separator + sfPathName.format(new Date()); //유동적인 경로(db저장경로)
//			
//			
//			String uploadPath = "newsPhoto" + File.separator + sfPathName.format(new Date());
//			String path = defaultPath +"/"+ uploadPath; //기본 + 유동
//			//String uriPath = "/Users/kwonjo/git/eduline/web/upload";
//			
//			// 저장경로 처리
//			makePath(path);
//			logger.debug("path ===========>" + path);
//			
//			// 파일 저장명 처리 (20150702091941)
//			String newFileName = sfFileName.format(new Date()) + "_" + ((int) (Math.random() * 10000)); // 새로
//			// 파일이름
//			String orgFileName = multipartFile.getOriginalFilename(); // 실제 파일이름
//			String ext = orgFileName.substring(orgFileName.lastIndexOf('.') + 1);
//			long fsize = multipartFile.getSize();
//			
//			news.setPhoto(uriPath+"/"+newFileName+"."+ext);
//			//fileUpload.setPath(uriPath);
//			//fileUpload.setExt(ext);
//		//	fileUpload.setNewName(newFileName);
//		//	fileUpload.setOrgName(orgFileName);
//	//		fileUpload.setSize(fsize);
//			
//			
//			
//			
//			
//			
//			// Multipart 처리
//			try {
//				// 서버에 파일 저장 (쓰기)
//				multipartFile.transferTo(new File(path + File.separator + newFileName +"."+ ext));
//				logger.debug("file name ====>" + path + File.separator + newFileName);
//			} catch (Exception e) {
//				e.printStackTrace();
//				logger.error("[ERROR]파일 업로드시에 오류가 발생하였습니다");
//			}
//			
//		}
//		return news;
//	}
//
//	
//	public FileUpload uploadIrFile(MultipartFile multipartFile, HttpSession httpSession) {
//		FileUpload fileUpload = new FileUpload();		
//		SimpleDateFormat sfPathName = new SimpleDateFormat(pattenDate);
//		SimpleDateFormat sfFileName = new SimpleDateFormat(pattenFile);
//		
//		
//		logger.debug("sfPathName ===> " + sfPathName.format(new Date()));
//		logger.debug("sfFileName ===> " + sfFileName.format(new Date()));
//		
//		// 업로드 파일이 존재하면
//		if (multipartFile != null && !(multipartFile.getOriginalFilename().equals(""))) {
//			// 파일크기 제한 (5MB)
//			long filesize = multipartFile.getSize(); // 파일크기
//			long limitFileSize = 40 * 1024 * 1024; // 5MB
//			
//			// 제한보다 파일크기가 클 경우
//			if (limitFileSize < filesize) {
//				return fileUpload;
//			}
//			//httpSession.getServletContext().getRealPath("/");
//			// 저장경로
//			
//			String defaultPath =  path;// 서버기본경로																
//			String uriPath = dbUploadPath +"IR" + File.separator + sfPathName.format(new Date()); //유동적인 경로(db저장경로)
//			
//			
//			String uploadPath = "IR" + File.separator + sfPathName.format(new Date());
//			String path = defaultPath +"/"+ uploadPath; //기본 + 유동
//			//String uriPath = "/Users/kwonjo/git/eduline/web/upload";
//			
//			// 저장경로 처리
//			makePath(path);
//			logger.debug("path ===========>" + path);
//			
//			// 파일 저장명 처리 (20150702091941)
//			String newFileName = sfFileName.format(new Date()) + "_" + ((int) (Math.random() * 10000)); // 새로
//			// 파일이름
//			String orgFileName = multipartFile.getOriginalFilename(); // 실제 파일이름
//			String ext = orgFileName.substring(orgFileName.lastIndexOf('.') + 1);
//			long fsize = multipartFile.getSize();
//			
//			
//			
//			
//			
//			//news.setPhoto(uriPath+"/"+newFileName+"."+ext);
//			fileUpload.setPath(uriPath);
//			fileUpload.setExt(ext);
//			fileUpload.setNewName(newFileName);
//			fileUpload.setOrgName(orgFileName);
//			fileUpload.setSize(fsize);
//			
//			
//			
//			
//			
//			
//			// Multipart 처리
//			try {
//				// 서버에 파일 저장 (쓰기)
//				multipartFile.transferTo(new File(path + File.separator + newFileName +"."+ ext));
//				logger.debug("file name ====>" + path + File.separator + newFileName);
//			} catch (Exception e) {
//				e.printStackTrace();
//				logger.error("[ERROR]파일 업로드시에 오류가 발생하였습니다");
//			}
//			
//		}
//		return fileUpload;
//	}
	
	
	

//	public void downloadFile(HttpServletResponse response, HttpSession httpSession, FileUpload fileUpload) {
//		FileInputStream fis = null;
//		ServletOutputStream out = null;
//
//		try {
//			out = response.getOutputStream();
//
//			File file = new File(downPath +  File.separator+fileUpload.getDbUploadPath() + "/"
//					+ fileUpload.getNewName()+"."+fileUpload.getExt());
//			
//
//			if (!file.exists()) {
//				String str = "<script>alert('파일이 없습니다.'); history.back(-1);</script>";
//				response.setContentType("text/html; charset=UTF-8");
//				response.getOutputStream().write(str.getBytes());
//				return;
//			}
//
//			fis = new FileInputStream(file);
//
//      		String fileName = encodeFileName(fileUpload.getOrgName());
//
//			response.setHeader("Content-Type", "application/octet-stream; charset=utf-8");
//			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
//			response.setHeader("Content-Transfer-Encoding", "binary;");
//
//			response.setHeader("Pragma", "no-cache;");
//			response.setHeader("Expires", "-1;");
//
//			int byteRead = 0;
//			byte[] buffer = new byte[8192];
//			while ((byteRead = fis.read(buffer, 0, 8192)) != -1) {
//				out.write(buffer, 0, byteRead);
//			}
//
//			out.flush();
//		} catch (Exception e) {
//			// TODO: handle exception
//			throw new RuntimeException(e);
//		} finally {
//			if (out != null)
//				try {
//					out.close();
//				} catch (IOException e) {
//				}
//			if (fis != null)
//				try {
//					fis.close();
//				} catch (IOException e) {
//				}
//		}
//	}
}
