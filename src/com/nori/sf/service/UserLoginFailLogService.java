package com.nori.sf.service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nori.Core.domain.UserLoginFailLog;
import com.nori.sf.persistence.mysql.mybatis.mapper.UserLoginFailLogMapper;

@Service("userLoginFailLogService")
public class UserLoginFailLogService {

	@Autowired
	private UserLoginFailLogMapper userLoginFailLogMapper;

	public List<UserLoginFailLog> select() {
		return this.userLoginFailLogMapper.select();
	}

	public long count() {
		return this.userLoginFailLogMapper.count();
	}

	public int insert(String userId, HttpServletRequest request) {

		UserLoginFailLog failLog = new UserLoginFailLog();

		failLog.setTryDate(new Date());
		failLog.setUserId(userId);
		failLog.setAccessIp(request.getRemoteAddr());
		failLog.setUserAgent(request.getHeader("User-Agent"));
		failLog.setReferer(request.getHeader("referer"));

		Locale locale = request.getLocale();
		if (locale != null) {
			failLog.setLocale(locale.getLanguage());
			failLog.setCountry(locale.getCountry());
		}

		return this.userLoginFailLogMapper.insert(failLog);
	}

	public int delete(UserLoginFailLog userLoginFailLog) {
		return this.userLoginFailLogMapper.delete(userLoginFailLog);
	}

}
