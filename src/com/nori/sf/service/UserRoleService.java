package com.nori.sf.service;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import com.nori.Core.domain.UserRole;
import com.nori.Core.security.SecurityGrantedAuthority;
import com.nori.sf.persistence.mysql.mybatis.mapper.UserRoleMapper;

@Service("userRoleService")
public class UserRoleService {

	@Autowired
	private UserRoleMapper userRoleMapper;

	public void insertRole(List<GrantedAuthority> grantedAuthorities) {
		Iterator<GrantedAuthority> iterator = grantedAuthorities.iterator();

		while (iterator.hasNext()) {
			SecurityGrantedAuthority securityGrantedAuthority = (SecurityGrantedAuthority) iterator.next();
			UserRole role = new UserRole();
			role.setRoleCode(securityGrantedAuthority.getRole());
			role.setRoleName(securityGrantedAuthority.getName());
			role.setRoleType(securityGrantedAuthority.getType().getCode());
			insert(role);
		}
	}

	public List<UserRole> selectUserRoleList() {
		return this.userRoleMapper.selectUserRoleList();
	}

	public int insert(UserRole role) {
		return userRoleMapper.insert(role);
	}

	public int update(UserRole role) {
		return this.userRoleMapper.update(role);
	}

	public int delete(UserRole role) {
		return this.userRoleMapper.delete(role);
	}

}
