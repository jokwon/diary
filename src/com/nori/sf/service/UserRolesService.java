package com.nori.sf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nori.Core.domain.UserRoles;
import com.nori.sf.persistence.mysql.mybatis.mapper.UserRolesMapper;

@Service("userRolesService")
public class UserRolesService {

	@Autowired
	private UserRolesMapper userRolesMapper;

	public List<UserRoles> selectUserRolesList(String userId) {
		return this.userRolesMapper.selectUserRolesList(userId);
	}
	public UserRoles selectUserRoles(String userId) {
		return this.userRolesMapper.selectUserRoles(userId);
	}
	public int updateRoles(UserRoles userRoles) {
		return this.userRolesMapper.updateRoles(userRoles);
	}

	public int insert(UserRoles userRoles) {
		return this.userRolesMapper.insert(userRoles);
	}

	public int delete(UserRoles userRoles) {
		return this.userRolesMapper.delete(userRoles);
	}

}
