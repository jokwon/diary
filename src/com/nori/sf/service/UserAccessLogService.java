package com.nori.sf.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nori.Core.domain.User;
import com.nori.Core.domain.UserAccessLog;
import com.nori.sf.persistence.mysql.mybatis.mapper.UserAccessLogMapper;

@Service("userAccessLogService")
public class UserAccessLogService {

	@Autowired
	private UserAccessLogMapper userAccessLogMapper;

	public List<UserAccessLog> select() {
		return this.userAccessLogMapper.select();
	}

	public long count() {
		return this.userAccessLogMapper.count();
	}

	public int insert(User user, HttpServletRequest request) {

		UserAccessLog accessLog = new UserAccessLog();
		user.setUserAccessLog(accessLog);

		accessLog.setAccessDate(new Date());
		accessLog.setUserId(user.getUserId());
		accessLog.setAccessIp(request.getRemoteAddr());
		accessLog.setUserAgent(request.getHeader("User-Agent"));
		accessLog.setReferenceUri(request.getHeader("referer"));

		return this.userAccessLogMapper.insert(accessLog);
	}

	public int updateLogout(UserAccessLog userAccessLog) {
		return this.userAccessLogMapper.updateLogout(userAccessLog);
	}

	public int updateLogoutManualLog(UserAccessLog userAccessLog) {
		return this.userAccessLogMapper.updateLogoutManualLog(userAccessLog);
	}

	public int delete(UserAccessLog userAccessLog) {
		return this.userAccessLogMapper.delete(userAccessLog);
	}

	public int deleteByUserId(String userId) {
		return this.userAccessLogMapper.deleteByUserId(userId);
	}

}
