package com.nori.sf.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nori.Core.domain.MailCheckToken;
import com.nori.Core.domain.User;
import com.nori.sf.persistence.mysql.mybatis.mapper.UserMapper;

@Service("userService")
public class UserService {
	
	@Autowired
	private UserMapper userMapper;
	
	public User selectUserForLogin(String userId) {
		return this.userMapper.selectUserForLogin(userId);
	}
	
	public int insertUserInfo(User user) {
		return this.userMapper.insertUserInfo(user);
	}
	
	public int insertToken(MailCheckToken token) {
		return this.userMapper.insertToken(token);
	}
	
	public MailCheckToken selectToken(@Param("userId") String  userId, @Param("token") String token) {
		return this.userMapper.selectToken(userId, token);
	}
	
	public int updateJoinStatus(@Param("userId") String  userId, @Param("joinStatus") String joinStatus) {
		return this.userMapper.updateJoinStatus(userId, joinStatus);
	}
	
	public User selectUser(User user) {
		return this.userMapper.selectUser(user);
	}
	public List<User> selectListUser(User user) {
		return this.userMapper.selectListUser(user);
	}
	
	public List<User> selectUserList(User user) {
		return this.userMapper.selectUserList(user);
	}

	public List<User> selectListUserForSchedule(User user) {
		return this.userMapper.selectListUserForSchedule(user);
	}
	
	public int selectUserListCnt(User user) {
		return this.userMapper.selectUserListCnt(user);
	}
	
	
	
	public int insertUser(User user) {
		return this.userMapper.insertUser(user);
	}
	
	public User selectOneUser(int id) {
		return this.userMapper.selectOneUser(id);
	}
	
	public User selectUserEmail(String email) {
		return this.userMapper.selectUserEmail(email);
	}
	
	public int updateUser(User user) {
		return this.userMapper.updateUser(user);
	}
	public int deleteUser(User user) {
		return this.userMapper.deleteUser(user);
	}
}
